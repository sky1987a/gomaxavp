//
//  SurfaceData.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/24.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import Foundation
import SwiftyJSON

class SurfaceData {
    var index: Int              = 0
    var horizontalPosition: Int = 0
    var verticalPosition: Int   = 0
    var width: Int              = 0
    var height: Int             = 0
    
    init() {}
    init(json:JSON) {
        self.index              = json["index"].intValue
        self.horizontalPosition = json["horizontal_position"].intValue
        self.verticalPosition   = json["vertical_position"].intValue
        self.width              = json["width"].intValue
        self.height             = json["height"].intValue
    }
}
