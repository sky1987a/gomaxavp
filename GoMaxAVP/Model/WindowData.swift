//
//  WindowData.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/24.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import Foundation
import SwiftyJSON

class WindowData {
    var index: Int              = 0
    var horizontalPosition: Int = 0
    var verticalPosition: Int   = 0
    var width: Int              = 0
    var height: Int             = 0
    var horizontalOffset: Int   = 0
    var verticalOffset: Int     = 0
    var content: String         = ""
    var targetSurface: Int      = 0
    
    init() {}
    init(json:JSON){
        self.index                  = json["index"].intValue
        self.horizontalPosition     = json["horizontal_position"].intValue
        self.verticalPosition       = json["vertical_position"].intValue
        self.width                  = json["width"].intValue
        self.height                 = json["height"].intValue
        self.horizontalOffset       = json["horizontal_offset"].intValue
        self.verticalOffset         = json["vertical_offset"].intValue
        self.content                = json["content"].stringValue
        self.targetSurface          = json["target_surface"].intValue
    }
}
