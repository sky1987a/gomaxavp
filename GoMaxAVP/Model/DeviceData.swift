//
//  DeviceData.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/13.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import Foundation


class DeviceData {
    var mac         : String = ""
    var name        : String = ""
    var subName     : String = ""
    var chipsetType : String = ""
    var content     : String = ""
    var status      : Int = 0
    
    init() {}
    
    init(mac:String,chipsetType:String,content:String,status:Int) {
        self.mac = mac
        self.name = CoreDataManager.shared.getDeviceName(mac: mac)
        self.chipsetType = chipsetType
        if self.mac == self.name {
            var cType = ""
            if self.chipsetType.contains("AVP"){
                cType = "AVP"
            }else{
                cType = "AVX"
            }
            self.subName = "\(self.name) \(cType)"
        }else{
            self.subName = self.name
        }
        self.content = content
        self.status = status
    }
}
