//
//  Constants.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/13.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import Foundation

struct Constants {
    static let PARAMS_RESULT            =   "result"
    static let PARAMS_SUCCESS           =   "SUCCESS"
    static let PARAMS_DEVICES           =   "devices"
    static let PARAMS_DEVICE_ID         =   "device_id"
    static let PARAMS_IDENTITY          =   "identity"
    static let PARAMS_ENGINE            =   "engine"
    static let PARAMS_CHIPSET_TYPE      =   "chipset_type"
    static let PARAMS_FIRMWARE_CMD      =   "firmware_comment"
    static let PARAMS_VENDOR_ID         =   "vendor_id"
    static let PARAMS_PRODUCT_ID        =   "product_id"
    static let PARAMS_NODES             =   "nodes"
    static let PARAMS_STATUS            =   "status"
    static let PARAMS_IP                =   "ip"
    static let PARAMS_ADDRESS           =   "address"
    static let PARAMS_MESSAGE           =   "message"
    static let PARAMS_ERROR             =   "error"
    static let PARAMS_PROCESSING        =   "PROCESSING"
    static let PARAMS_REQUEST_ID        =   "request_id"
    static let PARAMS_ENCODER_DEVICE    =   "encoderDevice"
    static let PARAMS_DECODER_DEVICE    =   "decodeDevice"
    static let PARAMS_RESOLUTION_TYPE   =   "resolutionType"
    static let PARAMS_TYPE              =   "type"
    static let HDMI_ENCODER             =   "HDMI_ENCODER"
}

