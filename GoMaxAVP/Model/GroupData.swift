//
//  GroupData.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/23.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import Foundation
import SwiftyJSON
class GroupData {
    var name : String = ""
    var width: Int = 0
    var height: Int = 0
    var windows = [WindowData]()
    var surfaces = [SurfaceData]()
    init(){}
    init(name:String) {
        self.name = name
    }
}
