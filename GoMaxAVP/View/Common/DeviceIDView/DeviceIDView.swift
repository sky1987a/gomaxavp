//
//  DeviceIDView.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/6.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

protocol DeviceIDViewDelegate {
    func onDeviceIDViewSeleted(deviceIDView:DeviceIDView,index:Int, value:String)
}

class DeviceIDView: UIView {
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dropDownView: DropDownView!
    var delegate: DeviceIDViewDelegate?
    
    var deviceList:[DeviceData]!
    var selectIndex = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("DeviceIDView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        
        self.dropDownView.delegate = self
        self.makeBorder()
//
//        self.contentView.layer.cornerRadius = 10
//        self.contentView.layer.borderWidth = 1
//        self.contentView.layer.borderColor = UIColor.black.cgColor
        
        self.deviceList = [DeviceData]()
        
    }
    
    public func setData(deviceList:[DeviceData]){
        self.deviceList = deviceList
        var strs = [String]()
        for device in deviceList {
            strs.append(device.subName)
        }
        self.dropDownView.datas = strs
    }
    
    public func removeData(){
        self.dropDownView.datas = [String]()
    }
    
    public func getDeviceData() -> DeviceData {
        if self.deviceList.count > 0 {
            return self.deviceList[self.selectIndex]
        } else {
            return DeviceData()
        }
    }
    
//    public func getSelectedDeviceID() -> String {
//        return self.dropDownView.titleLabel.text!
//    }

}

extension DeviceIDView: DropDownViewDelegate{
    func onDropDownViewSelected(dropDownView: DropDownView, index: Int, value: String) {
        self.selectIndex = index
        self.delegate?.onDeviceIDViewSeleted(deviceIDView: self, index: index, value: value)
    }
}

