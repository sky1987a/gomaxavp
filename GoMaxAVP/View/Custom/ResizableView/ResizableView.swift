//
//  ResizableView.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/8.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

protocol ResizableViewDelegate {
    func onTouchBegan()
    func onTouchMove()
    func onTouchEnd()
}

class ResizableView: UIView {
    enum Edge {
        case topLeft, topRight, bottomLeft, bottomRight, none
    }

    var delegate: ResizableViewDelegate?
    static var edgeSize: CGFloat = 44.0
    private typealias `Self` = ResizableView

    var currentEdge: Edge = .bottomRight
    var touchStart = CGPoint.zero
    
    var maxWidth: CGFloat = 0.0
    var maxHeight: CGFloat = 0.0
    
    var circleView: UIView!
    
    public func initResizeView(){
        
        self.circleView = UIView.init(frame: CGRect.init(x: self.frame.maxX, y: self.frame.maxY, width: 10.0, height: 10.0))
        self.circleView = UIView()
        self.circleView.frame.size = CGSize.init(width: 20.0, height: 20.0)
        self.circleView.layer.borderColor = UIColor.lightGray.cgColor
        self.circleView.layer.borderWidth = 3.0
        self.circleView.layer.cornerRadius = 10.0
        
        self.circleView.frame.origin = CGPoint.init(x: self.frame.size.width + self.frame.origin.x - self.circleView.frame.size.width/2, y: self.frame.size.height + self.frame.origin.y - self.circleView.frame.size.height/2)
        
        self.addSubview(self.circleView)
        self.bringSubviewToFront(self.circleView)
    }
    
    public func setMaxData(width:CGFloat, height:CGFloat){
        self.maxWidth = width
        self.maxHeight = height
    }
    
    // 更新圓圈圈
    public func updateCycle(){
        self.circleView.frame.origin = CGPoint.init(x: 0 + self.frame.size.width - self.circleView.frame.size.width/2, y: 0 + self.frame.size.height  - self.circleView.frame.size.height/2)
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {

            touchStart = touch.location(in: self)
            
            // 判斷動作
            currentEdge = {
                if self.bounds.size.width - touchStart.x < Self.edgeSize && self.bounds.size.height - touchStart.y < Self.edgeSize {
                    return .bottomRight
                } else if touchStart.x < Self.edgeSize && touchStart.y < Self.edgeSize {
                    return .topLeft
                } else if self.bounds.size.width-touchStart.x < Self.edgeSize && touchStart.y < Self.edgeSize {
                    return .topRight
                } else if touchStart.x < Self.edgeSize && self.bounds.size.height - touchStart.y < Self.edgeSize {
                    return .bottomLeft
                }
                return .none
            }()
            self.delegate?.onTouchBegan()
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            let previous = touch.previousLocation(in: self)

            let width = self.frame.size.width
            let height = self.frame.size.height

            let deltaWidth = currentPoint.x - previous.x
            let deltaHeight = currentPoint.y - previous.y

            switch currentEdge {

            case .bottomRight:
                
                var newW = ceil(width + deltaWidth)
                var newH = ceil(height + deltaHeight)
                
                if ceil(self.frame.origin.x + width + deltaWidth) > self.maxWidth {
                    newW = self.maxWidth - self.frame.origin.x
                }else if ceil(width + deltaWidth) > self.maxWidth {
                    newW = self.maxWidth
                }
                
                if ceil(self.frame.origin.y + height + deltaHeight) > self.maxHeight {
                    newH = self.maxHeight - self.frame.origin.y
                }else if ceil(height + deltaHeight) > self.maxHeight {
                    newH = self.maxHeight
                }
                
                if newW <= 0{
                    newW = 0
                }
                if newH <= 0{
                    newH = 0
                }
                self.frame.size = CGSize.init(width: newW, height: newH)
                self.updateCycle()
            default:
                // Moving
                let dX = currentPoint.x - previous.x
                let dY = currentPoint.y - previous.y
                var newCX = ceil(self.frame.origin.x + dX)
                var newCY = ceil(self.frame.origin.y + dY)
            
                if ceil(newCX + width) >= self.maxWidth && dX > 0.0 {
                    newCX = self.maxWidth - width
                }
                
                if ceil(newCY + height) >= self.maxHeight && dY > 0.0 {
                   newCY = self.maxHeight - height
               }
                
                if newCX <= 0 && dX < 0.0 {
                    newCX = 0
                }
                
                if newCY <= 0 && dY < 0.0 {
                    newCY = 0
                }
                
                // special case
                if width >= self.maxWidth{
                    newCX = 0
                }
                if height >= self.maxHeight{
                    newCY = 0
                }
                
                self.frame.origin = CGPoint.init(x: newCX, y: newCY)
            }
            
            self.delegate?.onTouchMove()
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        currentEdge = .none
        self.delegate?.onTouchEnd()
    }
}
