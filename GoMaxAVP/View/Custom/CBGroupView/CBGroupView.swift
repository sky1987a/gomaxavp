//
//  CBGroupView.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/6.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

protocol CBGroupViewDelegate {
    func onCheckBoxSelected(cbGroupView: CBGroupView,items:[Item],selectedIndex:Int)
}

class CBGroupView: UIView {
    @IBOutlet weak var tableView: UITableView!
    var items:[Item]!
    var delegate: CBGroupViewDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tableView.register(UINib(nibName: "CBGroupTableViewCell", bundle: nil), forCellReuseIdentifier: "CBGroupTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.items = [Item]()
    }

    public func setData(items:[Item]){
        self.items = items
        self.tableView.reloadData()
    }
    
    public func setDefault(index:Int){
        var tmp = [Item]()
        for i in 0..<self.items.count{
            var item = self.items[i]
            if(i == index){
                item.selected = true
            }else{
                item.selected = false
            }
            tmp.append(item)
        }
        self.items = tmp
        self.tableView.reloadData()
    }
}

extension CBGroupView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var temp = [Item]()
        for i in 0..<self.items.count {
            var item = self.items[i]
            if(i == indexPath.row){
                item.selected = true
            } else {
                item.selected = false
            }
            temp.append(item)
        }
        self.items = temp
        self.tableView.reloadData()
        self.delegate?.onCheckBoxSelected(cbGroupView: self, items: self.items, selectedIndex: indexPath.row)
    }
}

extension CBGroupView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CBGroupTableViewCell", for: indexPath) as! CBGroupTableViewCell
        let item = self.items[indexPath.row]
        cell.titleLabel.text = item.title
        cell.checkBox.isChecked = item.selected
        
        return cell
    }
    
    
}
