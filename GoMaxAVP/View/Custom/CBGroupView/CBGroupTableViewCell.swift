//
//  CBGroupTableViewCell.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/6.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class CBGroupTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var checkBox: CheckBox!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.checkBox.style = .circle
        self.checkBox.borderStyle = .rounded
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
