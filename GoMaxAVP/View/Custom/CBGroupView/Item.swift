//
//  Item.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/6.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

struct Item {
    let title: String
    var selected: Bool
}
