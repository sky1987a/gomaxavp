//
//  CBGroupSuperView.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/6.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

protocol CBGroupSuperViewDelegate {
    func onCheckBoxSelected(cbGroupSuperView: CBGroupSuperView,items:[Item],selectedIndex:Int)
}

@IBDesignable class CBGroupSuperView: UIView {
    var cbGroupView : CBGroupView?
    var delegate: CBGroupSuperViewDelegate?
     override func prepareForInterfaceBuilder() {
         super.prepareForInterfaceBuilder()
         addXibView()
     }
     
     override func awakeFromNib() {
         super.awakeFromNib()
         addXibView()
     }
     
    
     func addXibView() {
         if let cbGroupView = Bundle(for: CBGroupView.self).loadNibNamed("\(CBGroupView.self)", owner: nil, options: nil)?.first as? CBGroupView {
            self.cbGroupView = cbGroupView
            self.cbGroupView?.delegate = self
            addSubview(cbGroupView)
            cbGroupView.frame = bounds
         }
     }
    
    /// Item資料
    public func setData(items:[Item]){
        self.cbGroupView?.setData(items: items)
    }
    
    /// 設定default
    public func setDefault(index:Int){
        self.cbGroupView?.setDefault(index: index)
    }
    

}

extension CBGroupSuperView: CBGroupViewDelegate {
    func onCheckBoxSelected(cbGroupView: CBGroupView, items: [Item], selectedIndex: Int) {
        self.delegate?.onCheckBoxSelected(cbGroupSuperView: self, items: items, selectedIndex: selectedIndex)
    }
}
