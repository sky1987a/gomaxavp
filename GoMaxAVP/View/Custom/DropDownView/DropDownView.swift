//
//  DropDownView.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/6.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

protocol DropDownViewDelegate {
    func onDropDownViewSelected(dropDownView:DropDownView,index:Int,value:String)
}

class DropDownView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    var delegate: DropDownViewDelegate?
    
    public var datas :[String]!{
        didSet{
            if datas.count > 0 {
                self.titleLabel.text = datas[0]
            } else {
                self.titleLabel.text = ""
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    func commonInit(){
        Bundle.main.loadNibNamed("DropDownView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }

    @IBAction func btnPressed(_ sender: Any) {
        if let datas = self.datas{
            if datas.count > 0 {
                let index = datas.index(of: self.titleLabel.text!)!
                ActionSheetStringPicker.show(withTitle: "",
                                     rows: datas,
                                     initialSelection: index,
                                     doneBlock: { picker, index, value in
                                        self.titleLabel.text = String(describing: value!)
                                        self.delegate?.onDropDownViewSelected(dropDownView:self, index: index, value: String(describing: value!))
                                        return
                                     },
                                     cancel: { picker in
                                        return
                                     },
                                     origin: sender)
            }

        }
    }
    
}
