//
//  AndroidButton.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/27.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

@IBDesignable
class AndroidButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.setBackgroundImage(UIColor.groupTableViewBackground.image(), for: .normal)
        self.setTitleColor(UIColor.black, for: .normal)
        self.titleLabel?.font = .boldSystemFont(ofSize: 12)
    }

}
