//
//  ChangeSourceDialogTableViewCell.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/9.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class ChangeSourceDialogTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
