//
//  BaseTableViewCell.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/22/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//
import UIKit

open class BaseTableViewCell : UITableViewCell {
//    class var identifier: String { return String.className(self) }
//
//    public required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setup()
//    }
//
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        setup()
//    }
//
//    open override func awakeFromNib() {}
//
//    open func setup() {}
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    open class func height() -> CGFloat {
        return 48
    }
    
    open func setData(_ data: Any?) {
//        self.backgroundColor = UIColor(hex: "F1F8E9")
        
        self.nameLabel?.font = UIFont.italicSystemFont(ofSize: 18)
        self.nameLabel?.textColor = UIColor(hex: "9E9E9E")
        if let d = data as? (String,String) {
            self.nameLabel?.text = d.0
            self.iconImageView?.image = UIImage(named: d.1)
        }
    }
    
    override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            self.alpha = 0.4
        } else {
            self.alpha = 1.0
        }
    }
    
    // ignore the default handling
    override open func setSelected(_ selected: Bool, animated: Bool) {
    }
  
}
