//
//  MutiTableViewCell.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/24.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class MultiDialogTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func deleteBtnPressed(_ sender: Any) {

    }

}
