//
//  MultiViewTableViewCell.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/18.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class MultiViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var contentLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.bgView.makeBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
