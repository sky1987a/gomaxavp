//
//  CmdHelper.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/13.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import Foundation

public enum CommendID: Int {
    case requireBlueriverAPI    = 1
//    case modeHuman              = 2
    case getAllList             = 3
    case getAllIdentity         = 4
    case startHDMI              = 5
    case stopHDMI               = 6
    case leaveHDMI              = 7
    case joinHDMIGerenal        = 8
    case joinHDMIGenlock        = 9
    case joinHDMIResolution     = 10
    case reboot                 = 11
    case factory                = 12
    case startHDMINum           = 13
    case stopHDMINum            = 14
    case setHY                  = 15
    case startHDMIAudio         = 16
    case stopHDMIAudio          = 17
    case leaveHDMIAudio         = 18
    case setHDMIAudioMode       = 19
    case joinHDMIAudio          = 20
    case joinI2SAudio           = 21
    case startI2SAudio          = 22
    case stopI2SAudio           = 23
    case leaveI2SAudio          = 24
    case setI2SAudioOutput      = 25
    case setUSBRole             = 26
    case getEDID                = 27
    case request1               = 28
    case setEDID                = 29
    case request2               = 30
    case requireMultiview       = 31
    case listLayout             = 32
    case layout1                = 33
    case layout2                = 34
    case layout3                = 35
    case layoutGroup            = 36
    case layoutCreate           = 37
    case layoutGroupDelete      = 38
    case setResolution          = 39
    case startHDMI1             = 40
    case setMultivewSetting     = 41
    case joinHDMI1              = 42
    case getDeviceSetting       = 99
    case deleteWindow           = 98
    
}
