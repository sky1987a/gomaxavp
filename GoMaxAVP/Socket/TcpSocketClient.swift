//
//  TcpSocketClient.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/13.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import Foundation
import CocoaAsyncSocket

protocol TcpSocketClientDeleage: class {
    func onConnect()
    func disConnect(err: String)
    func onReadData(data: Data,tag:Int)
}

class TcpSocketClient: NSObject {
    fileprivate var clientSocket: GCDAsyncSocket!
    
    //重連時間間隔
    fileprivate var timeInterval = 1;
    
    //設定Timeout  -1為持續等待
    fileprivate let socketTimeout = 10.0
    
    static let sharedInstance=TcpSocketClient();
    weak var delegate: TcpSocketClientDeleage?
    private override init() {
        super.init();
        clientSocket = GCDAsyncSocket(delegate: self, delegateQueue: DispatchQueue.main)
    }
}

extension TcpSocketClient {
    // 開啟連接
    func startConnect(){
        startReConnectTimer();
    }
    
    // 斷開連接
    func stopConnect(){
        if(clientSocket.isConnected){
            clientSocket.disconnect()
        }
    }
    
    
    // 啟動連接
    func startReConnectTimer(){
        
        if(self.clientSocket.isDisconnected){
            GCDTimer.shared.scheduledDispatchTimer(WithTimerName: "reconnect", timeInterval: Double(timeInterval), queue: .main, repeats: false) {
                print("連接中...")
                
                do {
                    let ip = UserDefaults.standard.string(forKey: "IP_ADDRESS")
                    try self.clientSocket.connect(toHost: ip ?? "127.0.0.1", onPort: 6970, withTimeout: 5)
                } catch {
                    print(error)
                }
            }
            
        } else {
            self.delegate?.onConnect()
        }
        
    }
    
    func stopReConnectTimer(){
        GCDTimer.shared.cancleTimer(WithTimerName: "reconnect")
    }
    
}

extension TcpSocketClient {
    // 發送消息
    func sendMessage(cid:CommendID){
        var cmd = ""
        switch cid {
        case .requireBlueriverAPI:
            cmd = "require blueriver_api 2.19.0\n"
//        case .modeHuman:
//            cmd = "mode human\n"
        case .getAllIdentity:
            cmd = "get all identity\n"
        case .requireMultiview:
            cmd = "require multiview 1.1.0\n"
        case .listLayout:
            cmd = "list layout\n"
        default:
            cmd = ""
        }
        if clientSocket.isConnected {
            print(cmd)
            clientSocket.write(cmd.data(using: .utf8), withTimeout: socketTimeout, tag: cid.rawValue)
            clientSocket.readData(to: GCDAsyncSocket.lfData(), withTimeout: socketTimeout, tag: cid.rawValue)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.delegate?.disConnect(err: "Timeout")
            }
        }

    }
    
    func sendMsg(cmd:String){
        if clientSocket.isConnected {
            print(cmd)
            clientSocket.write(cmd.data(using: .utf8), withTimeout: socketTimeout, tag: 111)
            clientSocket.readData(to: GCDAsyncSocket.lfData(), withTimeout: socketTimeout, tag: 111)
        } 
    }
    
    func sendMessageWithParams(cid:CommendID , params:[String:String]){
        var cmd = ""
        switch cid {
        case .startHDMI:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "start \(deviceID):HDMI:0\n"
        case .stopHDMI:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "stop \(deviceID):HDMI:0\n"
        case .leaveHDMI:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "leave \(deviceID):HDMI:0\n"
        case .joinHDMIGerenal:
            let enDeviceID = params[Constants.PARAMS_ENCODER_DEVICE]!
            let deDevcieID = params[Constants.PARAMS_DECODER_DEVICE]!
            cmd = "join \(enDeviceID):HDMI:0 \(deDevcieID):HDMI:0\n"
        case .joinHDMIGenlock:
            let enDeviceID = params[Constants.PARAMS_ENCODER_DEVICE]!
            let deDevcieID = params[Constants.PARAMS_DECODER_DEVICE]!
            cmd = "join \(enDeviceID):HDMI:0 \(deDevcieID):HDMI:0 genlock\n"
        case .joinHDMIResolution:
            let enDeviceID = params[Constants.PARAMS_ENCODER_DEVICE]!
            let deDevcieID = params[Constants.PARAMS_DECODER_DEVICE]!
            let reSolutionType = params[Constants.PARAMS_RESOLUTION_TYPE]!
            var reType = ""
            switch reSolutionType {
            case "0":
                reType = "3840 2160 fps 60"
            case "1":
                reType = "1920 1080 fps 60"
            case "2":
                reType = "1280 720 fps 60"
            default:
                reType = ""
            }
            cmd = "join \(enDeviceID):HDMI:0 \(deDevcieID):HDMI:0 fastswitch size \(reType)\n"
        case .startHDMIAudio:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "start \(deviceID):HDMI_AUDIO:0\n"
        case .stopHDMIAudio:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "stop \(deviceID):HDMI_AUDIO:0\n"
        case .leaveHDMIAudio:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "leave \(deviceID):HDMI_AUDIO:0\n"
        case .setHDMIAudioMode:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            let status   = params["number"]!
            cmd = "set \(deviceID) property nodes[HDMI_ENCODER:0].inputs[audio:0].configuration.source.value \(status)\n"
        case .joinHDMIAudio:
            let enDeviceID = params[Constants.PARAMS_ENCODER_DEVICE]!
            let deDevcieID = params[Constants.PARAMS_DECODER_DEVICE]!
            cmd = "join \(enDeviceID):HDMI_AUDIO:0 \(deDevcieID):0\n"
        case .joinI2SAudio:
            let enDeviceID = params[Constants.PARAMS_ENCODER_DEVICE]!
            let deDevcieID = params[Constants.PARAMS_DECODER_DEVICE]!
            cmd = "join \(enDeviceID):I2S_AUDIO:0 \(deDevcieID):0\n"
        case .setI2SAudioOutput:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            let status   = params["number"]!
            cmd = "set \(deviceID) property nodes[I2S_AUDIO_OUTPUT:0].inputs[main:0].configuration.source.value \(status)\n"
        case .startI2SAudio:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "start \(deviceID):I2S_AUDIO:0\n"
        case .stopI2SAudio:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "stop \(deviceID):I2S_AUDIO:0\n"
        case .leaveI2SAudio:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "leave \(deviceID):I2S_AUDIO:0\n"
        case .setUSBRole:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            let usbRole  = params["USB_ROLE"]!
            cmd = "set \(deviceID) usb role \(usbRole)\n"
        case .setHY:
            let deviceID  = params[Constants.PARAMS_DEVICE_ID]!
            let oriWidth  = params["OriWidth"]!
            let oriHeihgt = params["OriHeight"]!
            let width     = params["Width"]!
            let height    = params["Height"]!
            let hStart    = params["HStart"]!
            let vStart    = params["VStart"]!
            cmd = "set \(deviceID) video wall size \(oriWidth) \(oriHeihgt) fps 60 offset \(hStart) \(vStart) keep \(width) \(height)\n"
        case .getEDID:
            let deviceID  = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "get \(deviceID) edid\n"
        case .request1:
            let requestID = params["request_id"]!
            cmd = "request \(requestID)\n"
        case .setEDID:
            let deviceID  = params[Constants.PARAMS_DEVICE_ID]!
            let edid      = params["EDID"]!
            cmd = "set \(deviceID) edid \(edid)\n"
        case .request2:
            let requestID = params["request_id"]!
            cmd = "request \(requestID)\n"
        case .layout1:
            let groupName = params["GroupName"]!
            cmd = "layout \(groupName) describe\n"
        case .getDeviceSetting:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "get \(deviceID) settings\n"
        case .layoutCreate:
            let groupName = params["GroupName"]!
            let width = params["Width"]!
            let height = params["Height"]!
            cmd = "layout \(groupName) create size \(width) \(height)\n"
        case .layoutGroupDelete:
            let groupName = params["GroupName"]!
            cmd = "layout \(groupName) delete\n"
        case .layout2:
            let groupName = params["GroupName"]!
            let windowNum = params["WindowNum"]!
            let hStart = params["HStart"]!
            let vStart = params["VStart"]!
            let width  = params["Width"]!
            let height = params["Height"]!
            let sourceNum = params["SourceNum"]!
            cmd = "layout \(groupName) window \(windowNum) position \(hStart) \(vStart) size \(width) \(height) target \(sourceNum)\n"
        case .deleteWindow:
            let groupName = params["GroupName"]!
            let windowNum = params["WindowNum"]!
            cmd = "layout \(groupName) window \(windowNum) delete\n"
        case .setResolution:
            let encoder = params["Encoder"]!
            let width  = params["Width"]!
            let height = params["Height"]!
            cmd = "set \(encoder) scaler size \(width) \(height)\n"
        case .startHDMI1:
            let encoder = params["Encoder"]!
            cmd = "start \(encoder):HDMI:1\n"
        case .setMultivewSetting:
            let decoder = params["Decoder"]!
            let groupName = params["GroupName"]!
            cmd = "set \(decoder) multiview \(groupName) fps 60\n"
        case .joinHDMI1:
            let encoder = params["Encoder"]!
            let decoder = params["Decoder"]!
            let surfaceID = params["SurfaceID"]!
            cmd = "join \(encoder):HDMI:1 \(decoder):\(surfaceID)\n"
        case .reboot:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "reboot \(deviceID)\n"
        case .factory:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            cmd = "factory \(deviceID)\n"
        case .startHDMINum:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            let num = params["number"]!
            cmd = "start \(deviceID):HDMI:\(num)\n"
        case .stopHDMINum:
            let deviceID = params[Constants.PARAMS_DEVICE_ID]!
            let num = params["number"]!
            cmd = "stop \(deviceID):HDMI:\(num)\n"
        default:
            cmd = ""
        }
        if clientSocket.isConnected {
            print(cmd)
            clientSocket.write(cmd.data(using: .utf8), withTimeout: socketTimeout, tag: cid.rawValue)
            clientSocket.readData(to: GCDAsyncSocket.lfData(), withTimeout: socketTimeout, tag: cid.rawValue)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.delegate?.disConnect(err: "Timeout")
            }
        }
    }
}

extension TcpSocketClient: GCDAsyncSocketDelegate {
    
    func socket(_ sock: GCDAsyncSocket, shouldTimeoutReadWithTag tag: Int, elapsed: TimeInterval, bytesDone length: UInt) -> TimeInterval {
        return elapsed
    }
    
    func socket(_ sock: GCDAsyncSocket, shouldTimeoutWriteWithTag tag: Int, elapsed: TimeInterval, bytesDone length: UInt) -> TimeInterval {
        return elapsed
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        
        let date = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let strNowTime = timeFormatter.string(from: date) as String
        print(strNowTime)
        if let e = err{
            self.delegate?.disConnect(err: e.localizedDescription)
        }
    }
    
    func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        let address = "Server IP：" + "\(host)"
        print(address)
        let date = Date()
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let strNowTime = timeFormatter.string(from: date) as String
        print(strNowTime)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.sendMessage(cid: .requireBlueriverAPI)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.sendMessage(cid: .requireMultiview)
        }

//        sock.readData(withTimeout: -1, tag: 0)
    }
    
    
    
    // 接收到消息
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        if (tag == CommendID.requireBlueriverAPI.rawValue){
            self.delegate?.onConnect()
        }
        
        if (tag == CommendID.getDeviceSetting.rawValue){
            print(data.count)
        }
        
        self.delegate?.onReadData(data: data, tag: tag)
//        sock.readData(withTimeout: -1, tag: 0);
    }
}
