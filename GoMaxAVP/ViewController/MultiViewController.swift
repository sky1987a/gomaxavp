//
//  MultiViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/18.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

struct MItem {
    var title:String = ""
    var content:String = ""
    var enabled: Bool =  true
}

public enum MultiType: Int {
    case group    = 0
    case window   = 1
    case surface  = 2
    case device   = 3
}

class MultiViewController: BaseViewController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var deviceNameTF: UITextField!
    @IBOutlet weak var resolutionView: UIView!
    @IBOutlet weak var resolutionLabel: UILabel!
    @IBOutlet weak var drawView: UIView!
    @IBOutlet weak var resizeView: ResizableView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var deviceData: DeviceData!
    var devices:[DeviceData]!
    var groups: [GroupData]!
    var mItems:[MItem]!
    
    var groupIndex = 0
    var windowsIndex = 0
    var surfaceIndex = 0
    var deviceIndex = 0
    
    var window: WindowData!
    var isFrist = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.deviceNameTF.text = self.deviceData.name
        
        self.groups = [GroupData]()
        
        self.mItems = [MItem]()
        self.mItems.append(MItem(title: "Group", content: ""))
        self.mItems.append(MItem(title: "Windows", content: ""))
        self.mItems.append(MItem(title: "Surface", content: ""))
        self.mItems.append(MItem(title: "Source", content: ""))
        
        self.resolutionView.makeBorder()

        self.resizeView.delegate = self
        
        self.window = WindowData()
        
        self.devices.remove(at: self.devices.firstIndex{$0 === self.deviceData}!)
        self.resizeView.initResizeView()
        loadFile()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.resizeView.setMaxData(width: self.drawView.frame.size.width, height: self.drawView.frame.size.height)
        if self.isFrist == false {
            self.resizeView.circleView.center = CGPoint.init(x: self.resizeView.frame.maxX - 5, y: self.resizeView.frame.maxY - 5)
            self.isFrist = true
        }

    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Multiview"
        
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.startConnect()
        self.showLoadingView()

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.dismiss(animated: true, completion: nil)
        self.view.endEditing(true)
    }

    @IBAction func saveBtnPressed(_ sender: Any) {
        let name = self.deviceNameTF.text!
        if name == "" {
            self.view.makeToast("Name is empty")
        } else {
            CoreDataManager.shared.insertData(mac: self.deviceData.mac, name: name)
            self.view.makeToast("Success")
        }
    }

    
    @IBAction func applyBtnPressed(_ sender: Any) {
        let encoder = self.devices[self.deviceIndex].mac
        var params = [String:String]()
        params["Encoder"] = encoder
        params["Width"] = String(self.window.width)
        params["Height"] = String(self.window.height)
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .setResolution, params: params)
        self.showLoadingView()
    }
    
    @IBAction func saveWindowBtnPressed(_ sender: Any) {
        let group = self.groups[self.groupIndex]
        let surface = group.surfaces[self.surfaceIndex]
        if group.name == "compatibility_4k_2x2"{
            self.view.makeToast("Can't edid with default group")
        }else{
            if self.window.horizontalPosition + self.window.width > group.width {
                self.view.makeToast("Window's width bigger than resolution's width")
            } else if self.window.verticalPosition + self.window.height > group.height {
                self.view.makeToast("Window's height bigger than resolution's height")
            } else {
                var params = [String:String]()
                params["GroupName"] = group.name
                params["WindowNum"] = String(self.window.index)
                params["HStart"] = String(self.window.horizontalPosition)
                params["VStart"] = String(self.window.verticalPosition)
                params["Width"] = String(self.window.width)
                params["Height"] = String(self.window.height)
                params["SourceNum"] = String(surface.index)
                TcpSocketClient.sharedInstance.delegate = self
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .layout2, params: params)
                self.showLoadingView()
            }
        }
    }
    
    @IBAction func rebootBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .reboot, params: params)
        self.showLoadingView()
    }
    
    @IBAction func factoryResetBtnPressed(_ sender:Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .factory, params: params)
        self.showLoadingView()
    }
    
    @IBAction func startBtnPressed(_ sender:Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        params["number"] = "0"
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .startHDMINum, params: params)
        self.showLoadingView()
    }
    
    @IBAction func stopBtnPressed(_ sender:Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        params["number"] = "0"
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .stopHDMINum, params: params)
        self.showLoadingView()
    }
    
    @IBAction func start1BtnPressed(_ sender:Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        params["number"] = "1"
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .startHDMINum, params: params)
        self.showLoadingView()
    }
    
    @IBAction func stop1BtnPressed(_ sender:Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        params["number"] = "1"
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .stopHDMINum, params: params)
        self.showLoadingView()
    }
}

extension MultiViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.mItems[indexPath.row]
        if item.enabled {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: MultiDialogViewController.typeName) as! MultiDialogViewController
            vc.modalPresentationStyle = .custom
            switch indexPath.row {
            case 0:
                vc.multiType = .group
            case 1:
                vc.multiType = .window
            case 2:
                vc.multiType = .surface
            case 3:
                vc.multiType = .device
            default:
                vc.multiType = .none
            }
            vc.groups = self.groups
            vc.devices = self.devices
            vc.groupIndex = self.groupIndex
            vc.windowIndex = self.windowsIndex
            vc.surfaceIndex = self.surfaceIndex
            vc.deviceInex = self.deviceIndex
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension MultiViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MultiViewTableViewCell", for: indexPath) as! MultiViewTableViewCell
        let item = self.mItems[indexPath.row]
        cell.titleLabel.text = item.title
        cell.contentLabel.text = item.content
        if item.enabled {
            cell.contentLabel.textColor = UIColor.black
        } else {
            cell.contentLabel.textColor = UIColor.gray
        }
        return cell
    }
}

extension MultiViewController: TcpSocketClientDeleage {
    func onConnect() {
        TcpSocketClient.sharedInstance.sendMessage(cid: .listLayout)
    }
    
    func disConnect(err: String) {

    }
    
    func onReadData(data: Data, tag: Int) {
        switch tag {
        case CommendID.listLayout.rawValue:
            self.groups = self.parseGroupList(data:data)
            if self.groups.count > 0 {
                let group = self.groups[self.groupIndex]
                var params = [String:String]()
                params["GroupName"] = group.name
                TcpSocketClient.sharedInstance.delegate = self
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .layout1, params: params)
            }
        case CommendID.layout1.rawValue:
            self.groups[self.groupIndex] = self.parseGroupData(data: data)
            self.updateView()
            self.dismissLoadingView()
        case CommendID.layout2.rawValue:
            let isSuccess = self.parseSuccess(data: data)
            if isSuccess {
                self.saveNewWindows()
                self.view.makeToast("Save windwow success")
            } else {
                self.view.makeToast("Save windwow failure")
            }
            self.dismissLoadingView()
        case CommendID.setResolution.rawValue:
            let isSuccess = self.parseSuccess(data: data)
            if isSuccess {
                let encoder = self.devices[self.deviceIndex].mac
                var params = [String:String]()
                params["Encoder"] = encoder
                TcpSocketClient.sharedInstance.delegate = self
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .startHDMI1, params: params)
            } else {
                self.view.makeToast("Set scaler size failure")
                self.dismissLoadingView()
            }
        case CommendID.startHDMI1.rawValue:
            let isSuccess = self.parseSuccess(data: data)
            if isSuccess {
                let decoder = self.deviceData.mac
                let group = self.groups[self.groupIndex]
                var params = [String:String]()
                params["Decoder"] = decoder
                params["GroupName"] = group.name
                TcpSocketClient.sharedInstance.delegate = self
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .setMultivewSetting, params: params)
            } else {
                self.view.makeToast("Start HDMI1 failure")
                self.dismissLoadingView()
            }
        case CommendID.setMultivewSetting.rawValue:
            let isSuccess = self.parseSuccess(data: data)
            if isSuccess {
                let decoder = self.deviceData.mac
                let encoder = self.devices[self.deviceIndex].mac
                let group = self.groups[self.groupIndex]
                let surface = group.surfaces[self.surfaceIndex]
                var params = [String:String]()
                params["Decoder"] = decoder
                params["Encoder"] = encoder
                params["SurfaceID"] = String(surface.index)
                TcpSocketClient.sharedInstance.delegate = self
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .joinHDMI1, params: params)
            } else {
                self.view.makeToast("Set multiview failure")
                self.dismissLoadingView()
            }
        case CommendID.joinHDMI1.rawValue:
            let isSuccess = self.parseSuccess(data: data)
            if isSuccess {
                self.view.makeToast("Apply success")
            } else {
                self.view.makeToast("Apply failure")
            }
            self.dismissLoadingView()
        case CommendID.reboot.rawValue:
            self.parseData(data: data)
            self.dismissLoadingView()
        case CommendID.factory.rawValue:
            self.parseData(data: data)
            self.dismissLoadingView()
        case CommendID.startHDMINum.rawValue,
             CommendID.stopHDMINum.rawValue:
            self.parseData(data: data)
            self.dismissLoadingView()
        case 111:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                TcpSocketClient.sharedInstance.delegate = self
                TcpSocketClient.sharedInstance.startConnect()
            }
        default:
            print("Unknowed Tag")
            self.dismissLoadingView()
        }
    }
}

extension MultiViewController: MultiDialogViewControllerDelegate{
    func onCreateWindow(index: Int) {
        let group = self.groups[self.groupIndex]
        let window = WindowData()
        window.index = index
        window.horizontalPosition = 0
        window.verticalPosition = 0
        window.width = Int(CGFloat(group.width) / self.drawView.frame.size.width) * 100
        window.height = Int(CGFloat(group.height) / self.drawView.frame.size.height) * 100
        group.windows.append(window)
        self.windowsIndex = group.windows.count-1
        self.window = window
        self.updateView()
    }
    
    func onSelected(multiType: MultiType, groupIndex: Int, windowIndex: Int, surfaceIndex: Int, deviceIndex: Int) {
        self.groupIndex = groupIndex
        self.windowsIndex  = windowIndex
        self.surfaceIndex = surfaceIndex
        self.deviceIndex = deviceIndex
        
        switch multiType  {
        case .group:
            TcpSocketClient.sharedInstance.delegate = self
            TcpSocketClient.sharedInstance.startConnect()
            self.showLoadingView()
        case .window:
            TcpSocketClient.sharedInstance.delegate = self
            TcpSocketClient.sharedInstance.startConnect()
            self.showLoadingView()
        case .surface:
            self.updateView()
        case .device:
            self.updateView()
        }
    }
}

extension MultiViewController: ResizableViewDelegate {
    func onTouchBegan() {
        self.scrollView.isScrollEnabled = false
    }
    
    func onTouchMove() {
        let group = self.groups[self.groupIndex]
        let hScale: CGFloat = CGFloat(group.width) / self.drawView.frame.size.width
        let vScale: CGFloat = CGFloat(group.height) / self.drawView.frame.size.height
        
        let x = self.resizeView.frame.origin.x * hScale
        let y = self.resizeView.frame.origin.y * vScale
        let width = self.resizeView.frame.size.width * hScale
        let height = self.resizeView.frame.size.height * vScale
        
        self.window.horizontalPosition = Int(x)
        self.window.verticalPosition   = Int(y)
        self.window.width = Int(width)
        self.window.height = Int(height)
        
        self.resolutionLabel.text = "Source Resolution:\n\(group.width) * \(group.height)\nWindow info:\nX:\(self.window.horizontalPosition) Y:\(self.window.verticalPosition)\nWidth:\(self.window.width) Height:\(self.window.height)"
    }
    
    func onTouchEnd() {
        self.scrollView.isScrollEnabled = true
    }
}

extension MultiViewController {
    fileprivate func updateView(){
        let group = self.groups[self.groupIndex]
        
        self.mItems[0].content = "\(group.name)"
        if group.windows.count == 0 {
            self.mItems[1].content = ""
        }else{
            self.mItems[1].content = "Window \(group.windows[self.windowsIndex].index)"
            self.window = group.windows[self.windowsIndex]
        }
        
        if group.surfaces.count > 0 {
            self.mItems[2].content = "Surface \(group.surfaces[self.surfaceIndex].index)"
        }
        
        if self.devices.count > 0 {
            self.mItems[3].content = self.devices[self.deviceIndex].name
        }
        
        self.tableView.reloadData()
        self.showWindow()
    }
    
    fileprivate func showWindow(){
        let group = self.groups[self.groupIndex]
        
        let hScale: CGFloat = self.drawView.frame.size.width / CGFloat(group.width)
        let vScale: CGFloat = self.drawView.frame.size.height / CGFloat(group.height)
        
        let x = Int(CGFloat(self.window.horizontalPosition) * hScale)
        let y = Int(CGFloat(self.window.verticalPosition) * vScale)
        let width = Int(CGFloat(self.window.width) * hScale)
        let height = Int(CGFloat(self.window.height) * vScale)
        
        self.resizeView.frame = CGRect(x: x, y: y, width: width, height: height)
//        self.resizeView.circleView.center = CGPoint.init(x: self.resizeView.frame.maxX - 5, y: self.resizeView.frame.maxY - 5)
        self.resizeView.updateCycle()
        self.resolutionLabel.text = "Source Resolution:\n\(group.width) * \(group.height)\nWindow info:\nX:\(self.window.horizontalPosition) Y:\(self.window.verticalPosition)\nWidth:\(self.window.width) Height:\(self.window.height)"
    }
}

extension MultiViewController {
    func saveNewWindows(){
        let group = self.groups[self.groupIndex]
        let surface = group.surfaces[self.surfaceIndex]
        CoreDataManager.shared.insertCmd(cmd: "layout \(group.name) window \(self.window.index) position \(self.window.horizontalPosition) \(self.window.verticalPosition) size \(self.window.width) \(self.window.height) target \(surface.index)\n")
        CoreDataManager.shared.saveCmdToFile()
    }
    
    func loadFile(){
        let file = AppFile()
        let content = file.readFile(at: .Documents, withName: "multiview.txt")
        let commands = content.split(separator: "\n")
        for command in commands {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                TcpSocketClient.sharedInstance.sendMsg(cmd: "\(command)\n")
            }
        }
    }
    
}
