//
//  ChangeSourceDialogViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/9.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit
import SwiftyJSON
import Toast_Swift

class ChangeSourceDialogViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var type1: Int = 0
    var type2: Int = 0
    var deviceList : [DeviceData]!
    
    var deviceData : DeviceData!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.tableFooterView = UIView()
        self.titleLabel.text = self.deviceData.name
        TcpSocketClient.sharedInstance.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension ChangeSourceDialogViewController {
    @IBAction func startBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .startHDMI, params: params)
        self.showLoadingView()
        
    }
    @IBAction func stopBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .stopHDMI, params: params)
        self.showLoadingView()
    }
    @IBAction func leaveBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceData.mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .leaveHDMI, params: params)
        self.showLoadingView()
    }
}

extension ChangeSourceDialogViewController: TcpSocketClientDeleage {
    func onConnect() {
        
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        switch tag {
        case CommendID.startHDMI.rawValue,CommendID.stopHDMI.rawValue,CommendID.leaveHDMI.rawValue,CommendID.joinHDMIGerenal.rawValue,CommendID.joinHDMIGenlock.rawValue, CommendID.joinHDMIResolution.rawValue:
            self.parseData(data: data)
            self.dismissLoadingView()
        default:
            print("Unknowed Tag")
        }
    }
    
//    fileprivate func parseData(data:Data){
//        do{
//            let json = try JSON(data: data)
//            let status = json[Constants.PARAMS_STATUS].stringValue
//            if (status == Constants.PARAMS_PROCESSING){
//                self.view.makeToast(Constants.PARAMS_SUCCESS)
//                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
//                    self.dismiss(animated: true, completion: nil)
//                }
//                
//            } else if (status == Constants.PARAMS_SUCCESS){
//                let errorMsg = json[Constants.PARAMS_RESULT][Constants.PARAMS_ERROR][0][Constants.PARAMS_MESSAGE].stringValue
//                self.view.makeToast(errorMsg)
//            }
//        }catch{
//            self.view.makeToast(error.localizedDescription)
//        }
//    }
    

}

extension ChangeSourceDialogViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let device = self.deviceList[indexPath.row]
        var params = [String:String]()
        params[Constants.PARAMS_ENCODER_DEVICE] = device.mac
        params[Constants.PARAMS_DECODER_DEVICE] = self.deviceData.mac
        params[Constants.PARAMS_RESOLUTION_TYPE] = String(self.type2)
        if(self.type1 == 0){
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .joinHDMIGerenal, params: params)
        }else if(self.type1 == 1){
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .joinHDMIGenlock, params: params)
        }else if(self.type1 == 2){
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .joinHDMIResolution, params: params)
        }
        self.showLoadingView()
    }
}

extension ChangeSourceDialogViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.deviceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChangeSourceDialogTableViewCell") as! ChangeSourceDialogTableViewCell
        let deviceData = self.deviceList[indexPath.row]
        cell.titleLabel.text = deviceData.name
        return cell
    }
    

}
