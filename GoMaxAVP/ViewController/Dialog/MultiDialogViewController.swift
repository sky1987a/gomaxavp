//
//  MutiDialogViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/24.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit
protocol MultiDialogViewControllerDelegate {
    func onCreateWindow(index:Int)
    func onSelected(multiType:MultiType,groupIndex:Int,windowIndex:Int,surfaceIndex:Int,deviceIndex:Int)
}

class MultiDialogViewController: BaseViewController {
    
    var delegate: MultiDialogViewControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    
    var multiType: MultiType!
    var groups: [GroupData]!
    var devices:[DeviceData]!
    
    var groupIndex  = 0
    var windowIndex = 0
    var surfaceIndex = 0
    var deviceInex = 0
    
    @IBOutlet weak var ControlViewHeight: NSLayoutConstraint!
    @IBOutlet weak var groupNameTF: UITextField!
    @IBOutlet weak var widthTF: UITextField!
    @IBOutlet weak var heightTF: UITextField!
    @IBOutlet weak var centerBtn: AndroidButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.tableFooterView = UIView()
        switch self.multiType {
        case .group:
            self.titleLabel.text = "Group"
            self.subTitleLabel.text = "Select group"
            self.ControlViewHeight.constant = 70.0
            self.centerBtn.setTitle("Create", for: .normal)
            self.closeBtn.isHidden = false
        case .window:
            self.titleLabel.text = "Window"
            self.subTitleLabel.text = "Select window"
            self.ControlViewHeight.constant = 0.0
            self.centerBtn.setTitle("Create", for: .normal)
            self.closeBtn.isHidden = false
        case .surface:
            self.titleLabel.text = "Surface"
            self.subTitleLabel.text = "Select surface"
            self.ControlViewHeight.constant = 0.0
            self.centerBtn.setTitle("Close", for: .normal)
        case .device:
            self.titleLabel.text = "Source"
            self.ControlViewHeight.constant = 0.0
            self.centerBtn.setTitle("Close", for: .normal)
        default:
            self.titleLabel.text = ""
        }
        
        TcpSocketClient.sharedInstance.delegate = self
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.dismiss(animated: true, completion: nil)
        self.view.endEditing(true)
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func centerBtnpressed(_ sender: Any) {
        switch self.multiType {
        case .group:
            if self.groupNameTF.text == "" || self.widthTF.text == "" || self.heightTF.text == ""{
                self.view.makeToast("Lost required data")
            }else{
                var params = [String:String]()
                params["GroupName"] = self.groupNameTF.text!
                params["Width"] = self.widthTF.text!
                params["Height"] = self.heightTF.text!
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .layoutCreate, params: params)
                self.showLoadingView()
                
            }
        case .window:
            if self.groups[self.groupIndex].name == "compatibility_4k_2x2"{
                self.view.makeToast("Can't create with default group")
            } else if self.groups[self.groupIndex].windows.count == 12 {
                self.view.makeToast("window is limit to 12")
            } else {
                var index = 0
                let windows = self.groups[self.groupIndex].windows
                var isBreak = false
                if windows.count > 0 {
                    for i in 0..<windows.count{
                        let window = windows[i]
                        if i != window.index{
                            isBreak = true
                            index = i
                            break
                        }
                    }
                }
                
                if windows.count > 0 && isBreak == false {
                    index = windows.count
                }
                self.delegate?.onCreateWindow(index: index)
                self.dismiss(animated: true, completion: nil)
            }
        case .surface:
            self.dismiss(animated: true, completion: nil)
        case .device:
            self.dismiss(animated: true, completion: nil)
        default:
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension MultiDialogViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.multiType {
        case .group:
            self.groupIndex = indexPath.row
        case .window:
            self.windowIndex = indexPath.row
            self.surfaceIndex = self.groups[self.groupIndex].windows[self.windowIndex].targetSurface
        case .surface:
            self.surfaceIndex = indexPath.row
        case .device:
            self.deviceInex = indexPath.row
        default:
            print("")
        }
        self.delegate?.onSelected(multiType: self.multiType, groupIndex: self.groupIndex, windowIndex: self.windowIndex, surfaceIndex: self.surfaceIndex, deviceIndex: self.deviceInex)
        self.dismiss(animated: true, completion: nil)
    }
}

extension MultiDialogViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.multiType {
        case .group:
            return self.groups.count
        case .window:
            return self.groups[self.groupIndex].windows.count
        case .surface:
            return self.groups[self.groupIndex].surfaces.count
        case .device:
            return self.devices.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MultiDialogTableViewCell", for: indexPath) as! MultiDialogTableViewCell
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnPressed), for: .touchUpInside)
        cell.deleteBtn.tag = indexPath.row
        switch self.multiType {
        case .group:
            cell.deleteBtn.isHidden = false
            let group = self.groups[indexPath.row]
            cell.titleLabel.text = group.name
            if group.name == "compatibility_4k_2x2"{
                cell.deleteBtn.isHidden = true
            }
        case .window:
            cell.deleteBtn.isHidden = false
            let window = self.groups[self.groupIndex].windows[indexPath.row]
            cell.titleLabel.text = "Window \(window.index)"
            if self.groups[self.groupIndex].name == "compatibility_4k_2x2"{
                cell.deleteBtn.isHidden = true
            }
        case .surface:
            cell.deleteBtn.isHidden = true
            let surface = self.groups[self.groupIndex].surfaces[indexPath.row]
            cell.titleLabel.text = "Surface \(surface.index)"
        case .device:
            cell.deleteBtn.isHidden = true
            let device = self.devices[indexPath.row]
            cell.titleLabel.text = device.name
        default:
            cell.titleLabel.text = ""
        }
        return cell
    }
    
    
    @objc func deleteBtnPressed(sender: UIButton!) {
        let index = sender.tag
        switch self.multiType {
        case .group:
            let group = self.groups[index]
            var params = [String:String]()
            params["GroupName"] = group.name
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .layoutGroupDelete, params: params)
            self.showLoadingView()
        case .window:
            let group = self.groups[self.groupIndex]
            let window = group.windows[index]
            var params = [String:String]()
            params["GroupName"] = group.name
            params["WindowNum"] = String(window.index)
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .deleteWindow , params: params)
            self.showLoadingView()
            
        default:
            print("Unknowed")
        }
    }
    
}

extension MultiDialogViewController: TcpSocketClientDeleage {
    func onConnect() {
        
    }
    
    func disConnect(err: String) {
        
    }
    
    func onReadData(data: Data, tag: Int) {
        switch tag {
        case CommendID.layoutCreate.rawValue:
            self.dismissLoadingView()
            let isSuccess = self.parseSuccess(data: data)
            if isSuccess {
                self.saveNewGroup()
                self.view.makeToast("Create group success")
                self.delegate?.onSelected(multiType: self.multiType, groupIndex: self.groupIndex, windowIndex: 0, surfaceIndex: 0, deviceIndex: self.deviceInex)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.view.makeToast("Create group failure")
            }
        case CommendID.layoutGroupDelete.rawValue:
            self.dismissLoadingView()
            let isSuccess = self.parseSuccess(data: data)
            if isSuccess {
                self.view.makeToast("Delete group success")
                self.delegate?.onSelected(multiType: self.multiType, groupIndex: 0, windowIndex: 0, surfaceIndex: 0, deviceIndex: 0)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.view.makeToast("Delete group failure")
            }
        case CommendID.deleteWindow.rawValue:
            self.dismissLoadingView()
            let isSuccess = self.parseSuccess(data: data)
            if isSuccess {
                self.view.makeToast("Delete Window success")
                self.delegate?.onSelected(multiType: self.multiType, groupIndex: self.groupIndex, windowIndex: 0, surfaceIndex: 0, deviceIndex: 0)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.view.makeToast("Delete Window failure")
            }
            
        default:
            print("Unknowed Tag")
        }
    }
}

extension MultiDialogViewController {
    func saveNewGroup(){
        CoreDataManager.shared.insertCmd(cmd: "layout \(self.groupNameTF.text!) create size \(self.widthTF.text!) \(self.heightTF.text!)\n")
        CoreDataManager.shared.saveCmdToFile()
    }
    
}
