//
//  FileDialogViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/17.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

protocol FileDialogViewControllerDelegate {
    func selectedFileContent(content:String)
}

class FileDialogViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var items:[String]!
    
    var delegate: FileDialogViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.items = [String]()
        self.tableView.tableFooterView = UIView()
        loadFiles()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func loadFiles(){
        let file = AppFile()
        self.items = file.getFileNameList()
        self.items = self.items.sorted().reversed()
        self.tableView.reloadData()
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension FileDialogViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let fileName = self.items[indexPath.row]
            let file = AppFile()
            let isDeleted = file.deleteFile(at: .Documents, withName: fileName)
            if isDeleted == true {
                self.items = file.getFileNameList()
                self.items = self.items.sorted().reversed()
                self.tableView.reloadData()
            } else {
                self.view.makeToast("Delete file failure")
            }

            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FileDialogTableViewCell", for: indexPath) as! FileDialogTableViewCell
        let fileName = self.items[indexPath.row]
        cell.titleLabel.text = fileName
        return cell
    }
    
    
}

extension FileDialogViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fileName = self.items[indexPath.row]
        let file = AppFile()
        let content = file.readFile(at: .Documents, withName: fileName)
        self.delegate?.selectedFileContent(content: content)
        self.dismiss(animated: true, completion: nil)
        
    }
}
