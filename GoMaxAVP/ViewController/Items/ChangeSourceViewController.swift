//
//  ChangeSourceViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChangeSourceViewController: ItemViewController {
    
    
    @IBOutlet weak var cbGroupView: CBGroupSuperView!
    @IBOutlet weak var cbGroupView1: CBGroupSuperView!
    @IBOutlet weak var resolutionView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var deviceList : [DeviceData]!
    
    var selectItem: Int = 0
    var selectItem1: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let items : [Item] = [
            Item(title: "Gerenal", selected: true),
            Item(title: "Genlock", selected: false),
            Item(title: "Fastswitch", selected: false)
        ]
        self.cbGroupView.setData(items: items)
        self.cbGroupView.delegate = self
        
        let items1 : [Item] = [
            Item(title: "4k2k 60", selected: true),
            Item(title: "1080p 60", selected: false),
            Item(title: "720p 60", selected: false)
        ]
        self.cbGroupView1.setData(items: items1)
        self.cbGroupView1.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Change Source"
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.startConnect()
        self.initData()
        self.showLoadingView()
    }
    
    fileprivate func initData(){
        self.deviceList = [DeviceData]()
        self.collectionView.reloadData()
    }
}

extension ChangeSourceViewController: TcpSocketClientDeleage {
    
    func onConnect() {
        TcpSocketClient.sharedInstance.sendMessage(cid: .getAllIdentity)
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        if (tag == CommendID.getAllIdentity.rawValue){
            self.deviceList = self.parseDeviceList(data: data,isOfflineDevcies: false)
            self.collectionView.reloadData()
            self.dismissLoadingView()
        }
    }
    
}

extension ChangeSourceViewController : CBGroupSuperViewDelegate {
    func onCheckBoxSelected(cbGroupSuperView: CBGroupSuperView, items: [Item], selectedIndex: Int) {
        if(cbGroupSuperView == self.cbGroupView){
            self.selectItem = selectedIndex
            if(selectedIndex == 2){
                self.resolutionView.isHidden = false
            } else {
                self.resolutionView.isHidden = true
            }
        } else if (cbGroupSuperView == self.cbGroupView1){
            self.selectItem1 = selectedIndex
        }
    }
}

extension ChangeSourceViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let deviceData = self.deviceList[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ChangeSourceDialogViewController.typeName) as! ChangeSourceDialogViewController
        vc.deviceData = deviceData
        vc.type1 = self.selectItem
        vc.type2 = self.selectItem1
        vc.deviceList = self.deviceList
        vc.modalPresentationStyle = .custom
        self.present(vc, animated: true, completion: nil)
    }
}

extension ChangeSourceViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.deviceList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChangeSourceCollectionViewCell", for: indexPath) as! ChangeSourceCollectionViewCell
        let device = self.deviceList[indexPath.row]
        cell.titleLabel.text = device.name
        cell.contentLabel.text = device.content
        if(device.status == 1){
            cell.titleLabel.backgroundColor = UIColor.WGreen
        } else {
            cell.titleLabel.backgroundColor = UIColor.WRed
        }
        return cell
    }
}

extension ChangeSourceViewController: UICollectionViewDelegateFlowLayout {
    
    /// 設定 Collection View 距離 Super View上、下、左、下間的距離
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }

    ///  設定 CollectionViewCell 的寬、高
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width - 30) / 2 , height: (self.view.frame.size.width - 30) / 2)
    }



    /// 滑動方向為「垂直」的話即「上下」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    /// 滑動方向為「垂直」的話即「左右」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
