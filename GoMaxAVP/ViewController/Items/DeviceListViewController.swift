//
//  DeviceListViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit
import SwiftyJSON


class DeviceListViewController: ItemViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var deviceList : [DeviceData]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Device List"
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.startConnect()
        self.initData()
        self.showLoadingView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.title = ""
    }
    
    fileprivate func initData(){
        self.deviceList = [DeviceData]()
        self.collectionView.reloadData()
    }
    
}

extension DeviceListViewController: TcpSocketClientDeleage {
    
    func onConnect() {
        TcpSocketClient.sharedInstance.sendMessage(cid: .getAllIdentity)
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        if (tag == CommendID.getAllIdentity.rawValue){
            self.deviceList = self.parseDeviceList(data: data,isOfflineDevcies: true)
            self.collectionView.reloadData()
            self.dismissLoadingView()
            if self.deviceList.count == 0 {
                self.view.makeToast("No connected device")
            }
        }
    }
    
}

extension DeviceListViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let deviceData = self.deviceList[indexPath.row]
        
        if deviceData.status == 1 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: MultiViewController.typeName) as! MultiViewController
            vc.deviceData = deviceData
            vc.devices = self.deviceList
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension DeviceListViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.deviceList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DeviceListCollectionViewCell", for: indexPath) as! DeviceListCollectionViewCell
        let deviceData = self.deviceList[indexPath.row]
        cell.titleLabel.text = deviceData.name
        cell.contentLabel.text = deviceData.content
        if(deviceData.status == 1){
            cell.titleLabel.backgroundColor = UIColor.WGreen
        } else {
            cell.titleLabel.backgroundColor = UIColor.WRed
        }
        return cell
    }
}

extension DeviceListViewController: UICollectionViewDelegateFlowLayout {
    
    /// 設定 Collection View 距離 Super View上、下、左、下間的距離
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }

    ///  設定 CollectionViewCell 的寬、高
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width - 30) / 2 , height: (self.view.frame.size.width - 30) / 2)
    }



    /// 滑動方向為「垂直」的話即「上下」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    /// 滑動方向為「垂直」的話即「左右」的間距(預設為重直)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}




