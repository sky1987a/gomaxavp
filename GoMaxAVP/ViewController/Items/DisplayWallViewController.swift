//
//  DisplayWallViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

struct DisplayData {
    var OriWidth: Int = 0
    var OriHeight: Int = 0
    var Width: Int = 0
    var Height: Int = 0
    var HStart: Int = 0
    var VStart: Int = 0
    var mode:String = ""
}

class DisplayWallViewController: ItemViewController {
    
    @IBOutlet weak var outputCBGroupView: CBGroupSuperView!
    @IBOutlet weak var deviceIDropDownView: DeviceIDView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var hCutDropDownView: DropDownView!
    @IBOutlet weak var vCutDropDownView: DropDownView!
    @IBOutlet weak var numberDropDownView: DropDownView!
    @IBOutlet weak var resizeableView: ResizableView!
    @IBOutlet weak var drawView:UIView!
    @IBOutlet weak var freeStyleSwitch: UISwitch!
    @IBOutlet weak var resetFreeStyleButton: AndroidButton!
    @IBOutlet weak var resolutionLabel: UILabel!
    @IBOutlet weak var widthLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var hStaartLabel: UILabel!
    @IBOutlet weak var vStartLabel: UILabel!
    
    var deviceList:[DeviceData]!
    var displayData: DisplayData!
    var tempDisplayData: DisplayData!
    
    var isFirst = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let items : [Item] = [
            Item(title: "4k2k 60", selected: true),
            Item(title: "1080p 60", selected: false),
            Item(title: "720p 60", selected: false)
        ]
        self.outputCBGroupView.setData(items: items)
        leftView.makeBorder()
        centerView.makeBorder()
        rightView.makeBorder()
        bottomView.makeBorder()
        
        hCutDropDownView.datas = ["1","2","3","4"]
        vCutDropDownView.datas = ["1","2","3","4"]
        numberDropDownView.datas = ["1"]
        
        self.hCutDropDownView.delegate = self
        self.vCutDropDownView.delegate = self
        self.numberDropDownView.delegate = self
        
//        self.resizeableView.layer.borderWidth = 1
//        self.resizeableView.layer.borderColor = UIColor.green.cgColor

        self.deviceIDropDownView.delegate = self
        
        freeStyleSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        self.resizeableView.isUserInteractionEnabled = false
        self.resizeableView.delegate = self
        
        self.displayData = DisplayData()
        self.tempDisplayData = DisplayData()

        self.deviceIDropDownView.backgroundColor = UIColor.WYellow
        self.leftView.backgroundColor = UIColor.WBlue
        self.centerView.backgroundColor = UIColor.WGreen
        self.rightView.backgroundColor = UIColor.white
        self.bottomView.backgroundColor = UIColor.WRed
        self.resizeableView.backgroundColor = UIColor.WGreen
        self.resizeableView.layer.borderColor = UIColor.init(hex: "79FF79").cgColor
        self.resizeableView.layer.borderWidth = 2.0
        self.resizeableView.initResizeView()
        self.resetFreeStyleButton.isHidden = true
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.resizeableView.setMaxData(width: self.drawView.frame.size.width, height: self.drawView.frame.size.height)
        
        if isFirst == false {
            self.resetResizeView()
            self.resizeableView.circleView.isHidden = true
            isFirst = true
        }
    }
     

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Display Wall"
        self.deviceIDropDownView.selectIndex = 0
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.startConnect()
        self.initData()
        self.showLoadingView()
        
    }
    
    fileprivate func initData(){
        self.deviceList = [DeviceData]()
        self.deviceIDropDownView.removeData()
    }
    
    
    @IBAction func applyBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        params["OriWidth"] = String(self.displayData.OriWidth)
        params["OriHeight"] = String(self.displayData.OriHeight)
        params["Width"] = String(self.displayData.Width)
        params["Height"] = String(self.displayData.Height)
        params["HStart"] = String(self.displayData.HStart)
        params["VStart"] = String(self.displayData.VStart)
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .setHY, params: params)
        self.showLoadingView()
    }
    
    @IBAction func freeStyleSwitchValueChanged(_ sender: UISwitch) {
        self.resizeableView.isUserInteractionEnabled = sender.isOn
        if sender.isOn{
            self.resizeableView.backgroundColor = UIColor.init(hex: "600000")
            self.resizeableView.layer.borderColor = UIColor.init(hex: "AE0000").cgColor
            self.resetResizeView()
            self.resizeableView.circleView.isHidden = false
            self.resetFreeStyleButton.isHidden = false
            
        } else {
            self.resizeableView.layer.borderColor = UIColor.init(hex: "79FF79").cgColor
            self.resizeableView.backgroundColor = UIColor.WGreen
            self.resetResizeView()
            self.resizeableView.circleView.isHidden = true
            self.resetFreeStyleButton.isHidden = true
            self.displayData = self.tempDisplayData
            self.updateDisplayData()
            self.updateView()
        }
    }
    
    @IBAction func resetFreeStyle(_ sender: Any) {
        self.resizeableView.backgroundColor = UIColor.init(hex: "600000")
        self.resizeableView.layer.borderColor = UIColor.init(hex: "AE0000").cgColor
        self.resetResizeView()
        self.resizeableView.circleView.isHidden = false
        
        // trigger onResizeViewFinish() for reset
        self.onTouchMove()
        self.updateDisplayData()
    }
    
    
    fileprivate func updateDisplayData(){
        self.displayData.OriWidth = 1920
        self.displayData.OriHeight = 1080
        self.widthLabel.text = "Width : \(self.displayData.Width)"
        self.heightLabel.text = "Height : \(self.displayData.Height)"
        self.hStaartLabel.text = "H Start : \(self.displayData.HStart)"
        self.vStartLabel.text = "V Start : \(self.displayData.VStart)"
        self.resolutionLabel.text = "\(self.displayData.OriWidth) * \(self.displayData.OriHeight) ( \(self.displayData.mode) )"
    }
}

extension DisplayWallViewController: TcpSocketClientDeleage {
    func onConnect() {
        TcpSocketClient.sharedInstance.sendMessage(cid: .getAllIdentity)
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        switch tag {
        case CommendID.getAllIdentity.rawValue:
            self.deviceList = self.parseDeviceList(data: data,isOfflineDevcies: false)
            self.deviceIDropDownView.setData(deviceList: self.deviceList)
            self.getDeviceSetting()
        case CommendID.getDeviceSetting.rawValue:
            let resolution = self.parseResolution(data: data)
            self.displayData.OriWidth = resolution.0
            self.displayData.OriHeight = resolution.1
            self.displayData.Width = resolution.0
            self.displayData.Height = resolution.1
            if(self.displayData.OriWidth == 3840){
                self.displayData.mode = "4k2k 60"
            } else if(self.displayData.OriWidth == 1920) {
                self.displayData.mode = "1080P 60"
            } else if(self.displayData.OriWidth == 1280) {
                self.displayData.mode = "720P 60"
            }
            self.updateDisplayData()
        case CommendID.setHY.rawValue:
            self.parseData(data: data)
        default:
            print("Unknowed Tag")
        }
        self.dismissLoadingView()
    }
}

extension DisplayWallViewController: DeviceIDViewDelegate{
    func onDeviceIDViewSeleted(deviceIDView: DeviceIDView, index: Int, value: String) {
        if(deviceIDView == self.deviceIDropDownView){
            self.getDeviceSetting()
        }
    }
    
    fileprivate func getDeviceSetting(){
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .getDeviceSetting, params: params)
        self.showLoadingView()
    }
}

extension DisplayWallViewController: DropDownViewDelegate{
    func onDropDownViewSelected(dropDownView: DropDownView, index: Int, value: String) {
        if(dropDownView != self.numberDropDownView){
            let hCut = Int(self.hCutDropDownView.titleLabel.text!)!
            let vCut = Int(self.vCutDropDownView.titleLabel.text!)!
            let num = hCut * vCut
            var tmp = [String]()
            for i in 1...num{
                tmp.append(String(i))
            }
            self.numberDropDownView.datas = tmp
        }
        self.calDisplayData()
    }
    
    func calDisplayData(){
        let hCut = Int(self.hCutDropDownView.titleLabel.text!)!
        let vCut = Int(self.vCutDropDownView.titleLabel.text!)!
        var num  = Int(self.numberDropDownView.titleLabel.text!)!
        let w = self.displayData.OriWidth / hCut
        let h = self.displayData.OriHeight / vCut
        
        num = num - 1
        let hStart = w * Int(num % hCut)
        let vStart = h * Int(num / hCut)
        
        self.displayData.Width  = w
        self.displayData.Height = h
        self.displayData.HStart = hStart
        self.displayData.VStart = vStart
        self.tempDisplayData = self.displayData
        self.updateDisplayData()
        self.updateView()
    }
    
    func updateView(){
        
        if self.displayData.OriWidth != 0 && self.displayData.OriHeight != 0 {
            let hScale: CGFloat = self.drawView.frame.size.width / CGFloat(self.displayData.OriWidth)
            let vScale: CGFloat = self.drawView.frame.size.height / CGFloat(self.displayData.OriHeight)
            
            let x = CGFloat(self.displayData.HStart) * hScale
            let y = CGFloat(self.displayData.VStart) * vScale
            let width = CGFloat(self.displayData.Width) * hScale
            let height = CGFloat(self.displayData.Height) * vScale
            self.resizeableView.frame = CGRect(x: x, y: y, width: width, height: height)
            self.resizeableView.updateCycle()
        }
    }
}

extension DisplayWallViewController: ResizableViewDelegate{
    func onTouchBegan() {
        
    }
    
    func onTouchMove() {
        let hScale: CGFloat = CGFloat(self.displayData.OriWidth) / self.drawView.frame.size.width
        let vScale: CGFloat = CGFloat(self.displayData.OriHeight) / self.drawView.frame.size.height
        
        let x :CGFloat = ceil(self.resizeableView.frame.origin.x * hScale)
        let y :CGFloat = ceil(self.resizeableView.frame.origin.y * vScale)
        let width :CGFloat = ceil(self.resizeableView.frame.size.width * hScale)
        let height :CGFloat = ceil(self.resizeableView.frame.size.height * vScale)
        
        self.displayData.HStart = Int(x)
        self.displayData.VStart = Int(y)
        self.displayData.Width = Int(width)
        self.displayData.Height = Int(height)
        
        // special case
        if self.displayData.OriWidth <  self.displayData.HStart + self.displayData.Width{
            self.displayData.HStart = self.displayData.OriWidth - self.displayData.Width
        }
        
        if self.displayData.OriHeight <  self.displayData.VStart + self.displayData.Height{
            self.displayData.VStart = self.displayData.OriHeight - self.displayData.Height
        }
        
        self.updateDisplayData()
    }
    
    func onTouchEnd() {
        
    }

}

extension DisplayWallViewController {
    fileprivate func resetResizeView(){
        self.resizeableView.frame = CGRect(x: 0, y: 0, width: self.drawView.frame.size.width / 2.0, height: self.drawView.frame.size.height / 2.0)
        self.resizeableView.updateCycle()
    }
}
