//
//  HDMIAudioViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class HDMIAudioViewController: ItemViewController {
    
    
    @IBOutlet weak var deviceIDropDownView: DeviceIDView!
    @IBOutlet weak var audioSourceDropDownView: DeviceIDView!
    
    @IBOutlet weak var hDMICBGroupView: CBGroupSuperView!
    @IBOutlet weak var hDMIView: UIView!
    
    var deviceList:[DeviceData]!
    var audioStatus = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.deviceIDropDownView.titleLabel.text = "Device ID"
        self.audioSourceDropDownView.titleLabel.text = "Audio Source"
        
        let items : [Item] = [
            Item(title: "HDMI audio (original audio from video subscription in)", selected: true),
            Item(title: "HDMI audio (stereo downmix)", selected: false),
            Item(title: "HDMI audio (all available channels)", selected: false),
            Item(title: "I2S audio subscription", selected: false),
            Item(title: "I2S audio local loop out", selected: false)
        ]
        self.hDMICBGroupView.setData(items: items)
        self.hDMICBGroupView.delegate = self
        
        hDMIView.makeBorder()
        
        self.deviceIDropDownView.delegate = self
        self.audioSourceDropDownView.delegate = self
        
        self.deviceIDropDownView.backgroundColor = UIColor.WYellow
        self.hDMIView.backgroundColor = UIColor.WBlue
        self.audioSourceDropDownView.backgroundColor = UIColor.WRed
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "HDMI Audio"
        self.deviceIDropDownView.selectIndex = 0
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.startConnect()
        self.initData()
        self.showLoadingView()
    }
    
    fileprivate func initData(){
        self.deviceList = [DeviceData]()
        self.deviceIDropDownView.removeData()
        self.audioSourceDropDownView.removeData()
    }

    
    @IBAction func startBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .startHDMIAudio, params: params)
        self.showLoadingView()
    }
    
    @IBAction func stopBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .stopHDMIAudio, params: params)
        self.showLoadingView()
    }
    
    @IBAction func leaveBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .leaveHDMIAudio, params: params)
        self.showLoadingView()
    }
    
}

extension HDMIAudioViewController: TcpSocketClientDeleage {
    func onConnect() {
        TcpSocketClient.sharedInstance.sendMessage(cid: .getAllIdentity)
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        switch tag {
        case CommendID.getAllIdentity.rawValue:
            self.deviceList = self.parseDeviceList(data: data,isOfflineDevcies: false)
            self.deviceIDropDownView.setData(deviceList: self.deviceList)
            self.audioSourceDropDownView.setData(deviceList: self.deviceList)
            self.getDeviceSetting()
        case CommendID.startHDMIAudio.rawValue,
             CommendID.stopHDMIAudio.rawValue,
             CommendID.leaveHDMIAudio.rawValue,
             CommendID.setHDMIAudioMode.rawValue,
             CommendID.joinHDMIAudio.rawValue,
             CommendID.joinI2SAudio.rawValue:
            self.parseData(data: data)
        case CommendID.getDeviceSetting.rawValue:
            self.audioStatus = self.getHDMIAudioStatus(data: data)
            switch self.audioStatus {
            case 2:
                self.hDMICBGroupView.setDefault(index: 0)
            case 6:
                self.hDMICBGroupView.setDefault(index: 1)
            case 7:
                self.hDMICBGroupView.setDefault(index: 2)
            case 8:
                self.hDMICBGroupView.setDefault(index: 3)
            case 9:
                self.hDMICBGroupView.setDefault(index: 4)
            default:
                self.hDMICBGroupView.setDefault(index: 0)
            }
        default:
            print("Unknowed Tag")
        }
        self.dismissLoadingView()
    }
}

extension HDMIAudioViewController: CBGroupSuperViewDelegate {
    func onCheckBoxSelected(cbGroupSuperView: CBGroupSuperView, items: [Item], selectedIndex: Int) {
        if(cbGroupSuperView == self.hDMICBGroupView){
            switch selectedIndex {
            case 0:
                self.audioStatus = 2
            case 1:
                self.audioStatus = 6
            case 2:
                self.audioStatus = 7
            case 3:
                self.audioStatus = 8
            case 4:
                self.audioStatus = 9
            default:
                self.audioStatus = 2
            }
            
            
            var params = [String:String]()
            params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
            params["number"] = String(self.audioStatus)
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .setHDMIAudioMode, params: params)
            self.showLoadingView()
        }
    }
}

extension HDMIAudioViewController: DeviceIDViewDelegate {
    func onDeviceIDViewSeleted(deviceIDView: DeviceIDView, index: Int, value: String) {
        if(deviceIDView == self.deviceIDropDownView){
            self.getDeviceSetting()
        } else if (deviceIDView == self.audioSourceDropDownView){
            var params = [String:String]()
            params[Constants.PARAMS_ENCODER_DEVICE] = self.deviceIDropDownView.getDeviceData().mac
            params[Constants.PARAMS_DECODER_DEVICE] = self.audioSourceDropDownView.getDeviceData().mac
            if (self.audioStatus > 7){
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .joinI2SAudio, params: params)
            } else {
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .joinHDMIAudio, params: params)
            }
            self.showLoadingView()
        }
    }
    
    fileprivate func getDeviceSetting(){
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .getDeviceSetting, params: params)
        self.showLoadingView()
    }
}
