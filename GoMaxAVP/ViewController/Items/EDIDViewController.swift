//
//  EDIDViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class EDIDViewController: ItemViewController {
    
    @IBOutlet weak var deviceIDropDownView: DeviceIDView!
    @IBOutlet weak var settingView: UIView!
    @IBOutlet weak var outputView: UIView!
    @IBOutlet weak var settingCBGroupView: CBGroupSuperView!
    @IBOutlet weak var eDIDTransferCheckBox: CheckBox!
    
    @IBOutlet weak var hDMIControlView: UIView!
    @IBOutlet weak var mOnitorControlView: UIView!
    
    @IBOutlet weak var textView: UITextView!
    
    var deviceList:[DeviceData]!
    
    var selected = 0
    var edidString = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.deviceIDropDownView.titleLabel.text = "Device ID"
        
        let items : [Item] = [
            Item(title: "HDMI Decoder EDID", selected: true),
            Item(title: "Monitor EDID", selected: false)
        ]
        self.settingCBGroupView.setData(items: items)
        self.settingCBGroupView.delegate = self
        
        self.settingView.makeBorder()
        self.outputView.makeBorder()
        
        self.eDIDTransferCheckBox.style = .tick
        self.eDIDTransferCheckBox.borderStyle = .square
        self.eDIDTransferCheckBox.delegate = self
    
        self.deviceIDropDownView.delegate = self
        self.edidString = self.textView.text!
        
        self.deviceIDropDownView.backgroundColor = UIColor.WYellow
        self.textView.backgroundColor = UIColor.WBlue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "EDID"
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.startConnect()
        self.deviceList = [DeviceData]()
        self.showLoadingView()
    }
    
    fileprivate func saveEdidToFile(content:String) -> Bool{
        let file = AppFile()
        return file.writeFile(containing: content, to: .Documents, withName: "\(generateCurrentTimeStamp()).txt")
    }
    
    fileprivate func generateCurrentTimeStamp () -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmss"
        return (formatter.string(from: Date()) as NSString) as String
    }
    
    @IBAction func updateEdidBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        params["EDID"] = self.edidString
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .setEDID, params: params)
        self.showLoadingView()
    }
    
    @IBAction func loadEdidBtnPressed(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: FileDialogViewController.typeName) as! FileDialogViewController
        vc.modalPresentationStyle = .custom
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func saveEdidBtnPressed(_ sender: Any) {
        let isSuccess = self.saveEdidToFile(content: self.edidString)
        if(isSuccess){
            self.view.makeToast("Success")
        }else{
            self.view.makeToast("Failure")
        }
    }
    
    @IBAction func shareEdidBtnPressed(_ sender: Any) {
        let activityVC = UIActivityViewController(activityItems: [self.edidString], applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
    }
    
}

extension EDIDViewController: CBGroupSuperViewDelegate{
    func onCheckBoxSelected(cbGroupSuperView: CBGroupSuperView, items: [Item], selectedIndex: Int) {
        if(cbGroupSuperView == self.settingCBGroupView){
            if(selectedIndex == 0){
                self.hDMIControlView.isHidden = false
                self.mOnitorControlView.isHidden = true
                self.selected = 0
            }else {
                self.hDMIControlView.isHidden = true
                self.mOnitorControlView.isHidden = false
                self.selected = 1
            }
            self.getEdid()
            self.eDIDTransferCheckBox.isChecked = false
        }
    }
}

extension EDIDViewController: TcpSocketClientDeleage {
    func onConnect() {
        TcpSocketClient.sharedInstance.sendMessage(cid: .getAllIdentity)
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        switch tag {
        case CommendID.getAllIdentity.rawValue:
            self.deviceList = self.parseDeviceList(data: data,isOfflineDevcies: false)
            self.deviceIDropDownView.setData(deviceList: self.deviceList)
            self.getEdid()
        case CommendID.getEDID.rawValue:
            let requestID = self.parseRequestID(data: data)
            var params = [String:String]()
            params["request_id"] = String(requestID)
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .request1, params: params)
        case CommendID.request1.rawValue:
            let edid = self.parseEDID(data: data,type:self.selected)
            self.edidString = edid
            self.textView.text = self.edidString
            self.edidString = "2d12d1"
        case CommendID.setEDID.rawValue:
            let requestID = self.parseRequestID(data: data)
            var params = [String:String]()
            params["request_id"] = String(requestID)
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .request2, params: params)
        case CommendID.request2.rawValue:
            let edid = self.parseEDID(data: data,type:self.selected)
            self.edidString = edid
            self.textView.text = self.edidString
        default:
            print("Unknowed Tag")
        }
        self.dismissLoadingView()
    }
    
    fileprivate func getEdid(){
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .getEDID, params: params)
        self.showLoadingView()
    }
}

extension EDIDViewController: DeviceIDViewDelegate {
    func onDeviceIDViewSeleted(deviceIDView: DeviceIDView, index: Int, value: String) {
        if(deviceIDView == self.deviceIDropDownView){
            self.eDIDTransferCheckBox.isChecked = false
            self.getEdid()
        }
    }
}

extension EDIDViewController: CheckBoxDelegate{
    func onCheckBoxChecked(isChecked: Bool) {
        if(isChecked){
            let string = self.edidString
            var newInts: Array<UInt8> = Array()
            let characters = Array(string)
            var subString = ""
            for character in characters {
                subString = subString + String(character)
                if(subString.count == 2){
                    newInts.append(UInt8(subString, radix: 16) ?? 0x00)
                    subString = ""
                }
            }
            
            let str = OCFile().parseEDID(intToUnsafeMutablePointer(array: newInts), withLen: 256)
            
            self.textView.text = String(cString: str)

        } else {
            self.textView.text = self.edidString
        }
    }
    
    func intToUnsafeMutablePointer(array:Array<UInt8>)  -> UnsafeMutablePointer<UInt8>{
        var initalArray = array
        let pointer = UnsafeMutablePointer<UInt8>(&initalArray)
        return pointer
    }
    
}

extension EDIDViewController: FileDialogViewControllerDelegate{
    func selectedFileContent(content: String) {
        self.edidString = content
        self.textView.text = self.edidString
    }
}
