//
//  I2SAudioViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class I2SAudioViewController: ItemViewController {
    
    @IBOutlet weak var deviceIDropDownView: DeviceIDView!
    @IBOutlet weak var audioSourceDropDownView: DeviceIDView!
    
    @IBOutlet weak var i2SOutputView: UIView!
    @IBOutlet weak var i2SInputView: UIView!
    
    @IBOutlet weak var i2SOutputCBGroupView: CBGroupSuperView!
    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var freqLabel: UILabel!
    @IBOutlet weak var channelLabel: UILabel!
    
    
    var deviceList:[DeviceData]!
    var i2SAudioStatus = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.deviceIDropDownView.titleLabel.text = "Device ID"
        self.audioSourceDropDownView.titleLabel.text = "Audio Source"
        
        let items : [Item] = [
            Item(title: "HDMI audio (stereo downmix) local loop out", selected: true),
            Item(title: "HDMI audio (all channels) local loop out", selected: false),
            Item(title: "I2S audio subscription (audio return channel)", selected: false)
        ]
        self.i2SOutputCBGroupView.setData(items: items)
        self.i2SOutputCBGroupView.delegate = self
        
        i2SOutputView.makeBorder()
        i2SInputView.makeBorder()
        
        self.deviceIDropDownView.delegate = self
        self.audioSourceDropDownView.delegate = self
        
        self.deviceIDropDownView.backgroundColor = UIColor.WYellow
        self.i2SOutputView.backgroundColor = UIColor.WBlue
        self.audioSourceDropDownView.backgroundColor = UIColor.WRed
        self.i2SInputView.backgroundColor = UIColor.WGreen
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "I2S Audio"
        self.deviceIDropDownView.selectIndex = 0
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.startConnect()
        self.initData()
        self.showLoadingView()
    }
    
    fileprivate func initData(){
        self.deviceList = [DeviceData]()
        self.deviceIDropDownView.removeData()
        self.audioSourceDropDownView.removeData()
        self.typeLabel.text = ""
        self.freqLabel.text = ""
        self.channelLabel.text = ""
    }

    @IBAction func startBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .startI2SAudio, params: params)
        self.showLoadingView()
    }
    
    @IBAction func stopBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .stopI2SAudio, params: params)
        self.showLoadingView()
    }
    
    @IBAction func leaveBtnPressed(_ sender: Any) {
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .leaveI2SAudio, params: params)
        self.showLoadingView()
    }

}

extension I2SAudioViewController: TcpSocketClientDeleage {
    func onConnect() {
        TcpSocketClient.sharedInstance.sendMessage(cid: .getAllIdentity)
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        switch tag {
        case CommendID.getAllIdentity.rawValue:
            self.deviceList = self.parseDeviceList(data: data,isOfflineDevcies: false)
            self.deviceIDropDownView.setData(deviceList: self.deviceList)
            self.audioSourceDropDownView.setData(deviceList: self.deviceList)
            self.getDeviceSetting()
        case CommendID.startI2SAudio.rawValue,
             CommendID.stopI2SAudio.rawValue,
             CommendID.leaveI2SAudio.rawValue,
             CommendID.setI2SAudioOutput.rawValue,
             CommendID.joinHDMIAudio.rawValue,
             CommendID.joinI2SAudio.rawValue:
            self.parseData(data: data)
        case CommendID.getDeviceSetting.rawValue:
            let result = self.parseI2SAudioStatus(data: data)
            self.typeLabel.text = result.0
            self.freqLabel.text = String(result.1)
            self.channelLabel.text = String(result.2)
            self.i2SAudioStatus = result.3
            print(self.i2SAudioStatus)
            switch self.i2SAudioStatus {
            case 6:
                self.i2SOutputCBGroupView.setDefault(index: 0)
            case 7:
                self.i2SOutputCBGroupView.setDefault(index: 1)
            case 8:
                self.i2SOutputCBGroupView.setDefault(index: 2)
            default:
                self.i2SOutputCBGroupView.setDefault(index: 0)
            }
        default:
            print("Unknowed Tag")
        }
        self.dismissLoadingView()
    }
}

extension I2SAudioViewController: CBGroupSuperViewDelegate {
    func onCheckBoxSelected(cbGroupSuperView: CBGroupSuperView, items: [Item], selectedIndex: Int) {
        if(cbGroupSuperView == self.i2SOutputCBGroupView){
            switch selectedIndex {
            case 0:
                self.i2SAudioStatus = 6
            case 1:
                self.i2SAudioStatus = 7
            case 2:
                self.i2SAudioStatus = 8
            default:
                self.i2SAudioStatus = 6
            }
            var params = [String:String]()
            params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
            params["number"] = String(self.i2SAudioStatus)
            TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .setI2SAudioOutput, params: params)
            self.showLoadingView()
        }
    }
}

extension I2SAudioViewController: DeviceIDViewDelegate {
    func onDeviceIDViewSeleted(deviceIDView: DeviceIDView, index: Int, value: String) {
        if(deviceIDView == self.deviceIDropDownView){
            self.getDeviceSetting()
        } else if (deviceIDView == self.audioSourceDropDownView){
            var params = [String:String]()
            params[Constants.PARAMS_ENCODER_DEVICE] = self.deviceIDropDownView.getDeviceData().mac
            params[Constants.PARAMS_DECODER_DEVICE] = self.audioSourceDropDownView.getDeviceData().mac
            if (self.i2SAudioStatus == 8){
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .joinI2SAudio, params: params)
            } else {
                TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .joinHDMIAudio, params: params)
            }
            self.showLoadingView()
        }
    }
    
    fileprivate func getDeviceSetting(){
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .getDeviceSetting, params: params)
        self.showLoadingView()
    }
}
