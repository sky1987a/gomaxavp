//
//  SettingViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class SettingViewController: ItemViewController {
    
    @IBOutlet weak var ipTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let ip = UserDefaults.standard.string(forKey: "IP_ADDRESS"){
            self.ipTextField.text = ip
        }
        
        if appUpdateAvailable() == true  {
            self.view.makeToast("App can updated")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Setting"
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }


    @IBAction func saveBtnPressed(_ sender: Any) {
        let userDefault = UserDefaults.standard
        userDefault.set(self.ipTextField.text!, forKey: "IP_ADDRESS")
        userDefault.synchronize()
        
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.stopConnect()
        TcpSocketClient.sharedInstance.startConnect()
        self.showLoadingView()
    }
    
}

extension SettingViewController:TcpSocketClientDeleage{
    func onConnect() {
        self.dismissLoadingView()
        self.view.makeToast("Connected")
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        
    }
}

extension SettingViewController : SlideMenuControllerDelegate {
    
}

extension SettingViewController {
    func appUpdateAvailable() -> Bool {
        let storeInfoURL: String = "http://itunes.apple.com/lookup?bundleId=\(Bundle.main.bundleIdentifier!)"
        print(storeInfoURL)
        var upgradeAvailable = false
        // Get the main bundle of the app so that we can determine the app's version number
        
        let bundle = Bundle.main
        if let infoDictionary = bundle.infoDictionary {
            // The URL for this app on the iTunes store uses the Apple ID for the  This never changes, so it is a constant
            let urlOnAppStore = URL(string: storeInfoURL)
            if let dataInJSON = NSData(contentsOf: urlOnAppStore!) {
                // Try to deserialize the JSON that we got
                if let dict: NSDictionary = try! JSONSerialization.jsonObject(with: dataInJSON as Data, options: .allowFragments) as! [String: AnyObject] as NSDictionary? {
                    if let results:NSArray = dict["results"] as? NSArray {
                        if results.count > 0 {
                            if let version = (results[0] as! NSDictionary).value(forKey: "version") as? String {
                                // Get the version number of the current version installed on device
                                if let currentVersion = infoDictionary["CFBundleShortVersionString"] as? String {
                                    // Check if they are the same. If not, an upgrade is available.
                                    print("\(version)")
                                    if version != currentVersion {
                                        upgradeAvailable = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return upgradeAvailable
    }
}
