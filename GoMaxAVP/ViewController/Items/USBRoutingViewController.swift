//
//  USBRoutingViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

class USBRoutingViewController: ItemViewController {
    
    @IBOutlet weak var deviceIDropDownView: DeviceIDView!
    @IBOutlet weak var settingView: UIView!
    @IBOutlet weak var settingCBGroupView: CBGroupSuperView!
    
    var deviceList:[DeviceData]!
    var usbRole = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.deviceIDropDownView.titleLabel.text = "Device ID"
        
        let items : [Item] = [
            Item(title: "Local", selected: true),
            Item(title: "Remote", selected: false),
            Item(title: "Disable", selected: false)
        ]
        self.settingCBGroupView.setData(items: items)
        self.settingCBGroupView.delegate = self
        
        self.settingView.makeBorder()

        self.deviceIDropDownView.delegate = self
        self.deviceIDropDownView.backgroundColor = UIColor.WYellow
        self.settingView.backgroundColor = UIColor.WBlue
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "USB Routing"
        TcpSocketClient.sharedInstance.delegate = self
        TcpSocketClient.sharedInstance.startConnect()
        self.initData()
        self.showLoadingView()
    }
    
    fileprivate func initData(){
        self.deviceList = [DeviceData]()
        self.deviceIDropDownView.removeData()
    }

}

extension USBRoutingViewController: TcpSocketClientDeleage {
    func onConnect() {
        TcpSocketClient.sharedInstance.sendMessage(cid: .getAllIdentity)
    }
    
    func disConnect(err: String) {
        self.dismissLoadingView()
        self.view.makeToast(err)
    }
    
    func onReadData(data: Data, tag: Int) {
        switch tag {
        case CommendID.getAllIdentity.rawValue:
            self.deviceList = self.parseDeviceList(data: data,isOfflineDevcies: false)
            self.deviceIDropDownView.setData(deviceList: self.deviceList)
            self.getDeviceSetting()
        case CommendID.getDeviceSetting.rawValue:
            self.usbRole = self.parseUSBRole(data:data)
            switch self.usbRole {
            case "LOCAL":
                self.settingCBGroupView.setDefault(index: 0)
            case "REMOTE":
                self.settingCBGroupView.setDefault(index: 1)
            case "DISABLED":
                self.settingCBGroupView.setDefault(index: 2)
            default:
                self.settingCBGroupView.setDefault(index: 2)
            }
        case CommendID.setUSBRole.rawValue:
            self.parseData(data: data)
        default:
            print("Unknowed Tag")
        }
        self.dismissLoadingView()
    }
}

extension USBRoutingViewController: DeviceIDViewDelegate{
    func onDeviceIDViewSeleted(deviceIDView: DeviceIDView, index: Int, value: String) {
        if(deviceIDView == self.deviceIDropDownView){
            self.getDeviceSetting()
        }
    }
    
    fileprivate func getDeviceSetting(){
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .getDeviceSetting, params: params)
        self.showLoadingView()
    }
}

extension USBRoutingViewController: CBGroupSuperViewDelegate {
    func onCheckBoxSelected(cbGroupSuperView: CBGroupSuperView, items: [Item], selectedIndex: Int) {
        if(cbGroupSuperView == self.settingCBGroupView){
            switch selectedIndex {
            case 0:
                self.usbRole = "LOCAL"
            case 1:
                self.usbRole = "REMOTE"
            case 2:
                self.usbRole = "DISABLED"
            default:
                self.usbRole = "DISABLED"
            }
        }
        var params = [String:String]()
        params[Constants.PARAMS_DEVICE_ID] = self.deviceIDropDownView.getDeviceData().mac
        params["USB_ROLE"] = self.usbRole
        TcpSocketClient.sharedInstance.sendMessageWithParams(cid: .setUSBRole, params: params)
        self.showLoadingView()
    }
}
