//
//  BaseViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/15.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftyJSON

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BaseViewController {
    public func showLoadingView() {
        print("showLoadingView~~~~~")
        DispatchQueue.main.async() {
                    SVProgressHUD.setDefaultMaskType(.black)
                            SVProgressHUD.show()
       }
    
    }
    
    public func dismissLoadingView() {
         print("dismissLoadingView~~~~~")
        DispatchQueue.main.async() {
                       SVProgressHUD.dismiss()
        }

    }
}

extension BaseViewController {
    public func parseDeviceList(data: Data, isOfflineDevcies:Bool) -> [DeviceData]{
        var deviceList = [DeviceData]()
        do{
            /// device_id
            /// identity.engine
            /// identity.chipset_type
            /// identity.firmware_comment
            /// identity.vendor_id , product_id
            ///
            let json =  try JSON(data: data)
            if(json[Constants.PARAMS_STATUS].stringValue == Constants.PARAMS_SUCCESS){
                
                if isOfflineDevcies == true {
                    if let errors = json[Constants.PARAMS_RESULT][Constants.PARAMS_ERROR].array {
                        for e in errors {
                            let mac = e[Constants.PARAMS_DEVICE_ID].stringValue
                            let content = e[Constants.PARAMS_MESSAGE].stringValue
                            
                            let deviceData = DeviceData(mac: mac,chipsetType: "", content: content, status: 0)
                            deviceList.append(deviceData)
                        }
                    }
                }
                
                if let devices = json[Constants.PARAMS_RESULT][Constants.PARAMS_DEVICES].array {
                    for device in devices {
                        
                        let deviceID = device[Constants.PARAMS_DEVICE_ID].stringValue
                        let engine = device[Constants.PARAMS_IDENTITY][Constants.PARAMS_ENGINE].stringValue
                        let chipsetType = device[Constants.PARAMS_IDENTITY][Constants.PARAMS_CHIPSET_TYPE].stringValue
                        let firmwareComment = device[Constants.PARAMS_IDENTITY][Constants.PARAMS_FIRMWARE_CMD].stringValue
                        let vendorID = device[Constants.PARAMS_IDENTITY][Constants.PARAMS_VENDOR_ID].stringValue
                        let productID = device[Constants.PARAMS_IDENTITY][Constants.PARAMS_PRODUCT_ID].stringValue
                        let ip = device[Constants.PARAMS_NODES][0][Constants.PARAMS_STATUS][Constants.PARAMS_IP][Constants.PARAMS_ADDRESS].stringValue
                        var result = ""
                        result += "\(engine)\n"
                        result += "\(chipsetType)\n"
                        result += "\(firmwareComment)\n"
                        result += "VID - \(vendorID) - PID - \(productID)\n"
                        result += ip
                        
                        let deviceData = DeviceData(mac: deviceID,chipsetType: chipsetType, content: result, status: 1)
                        if vendorID == "107"{
                            deviceList.append(deviceData)
                        }
                    }
                }
            }
        }catch{
            print("parseDeviceList error : \(error.localizedDescription)")
//            TcpSocketClient.sharedInstance.sendMessage(cid: .getAllIdentity)
        }
        return deviceList
    }
    
    public func parseGroupData(data:Data) -> GroupData {
        let group = GroupData()
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if (status == Constants.PARAMS_SUCCESS){
                let layoutDescription = json[Constants.PARAMS_RESULT]["layout_description"]
                let name = layoutDescription["name"].stringValue
                let width = layoutDescription["width"].intValue
                let height = layoutDescription["height"].intValue
                let surfaces = layoutDescription["surfaces"].arrayValue
                let windows = layoutDescription["windows"].arrayValue
                
                var ss = [SurfaceData]()
                for surface in surfaces{
                    ss.append(SurfaceData(json: surface))
                }
                
                var ws = [WindowData]()
                for window in windows{
                    ws.append(WindowData(json: window))
                }
                
                group.name = name
                group.width = width
                group.height = height
                group.windows = ws
                group.surfaces = ss
            }
        }catch{
            print("parseGroupData error : \(error.localizedDescription) ")
        }
        
        return group
    }
    
    public func parseGroupList(data:Data) -> [GroupData]{
        var groups = [GroupData]()
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if (status == Constants.PARAMS_SUCCESS){
                if let layouts = json[Constants.PARAMS_RESULT]["layout"].array{
                    for layout in layouts {
                        let group = GroupData()
                        group.name = layout["name"].stringValue
                        groups.append(group)
                    }
                }
            }
        }catch{
            print("parseGroupData error : \(error.localizedDescription) ")
        }
        
        return groups
    }
    
    public func parseSuccess(data:Data) -> Bool {
        var isSuccess = false
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if (status == Constants.PARAMS_SUCCESS || status == Constants.PARAMS_PROCESSING){
                isSuccess = true
            }
        }catch{
            print("parseSuccess error : \(error.localizedDescription)")
        }
        return isSuccess
    }
    
    public func parseData(data:Data){
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if (status == Constants.PARAMS_SUCCESS || status == Constants.PARAMS_PROCESSING){
                self.view.makeToast(Constants.PARAMS_SUCCESS)
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.dismiss(animated: true, completion: nil)
                }
                
            } else if (status == Constants.PARAMS_SUCCESS){
                let errorMsg = json[Constants.PARAMS_RESULT][Constants.PARAMS_ERROR][0][Constants.PARAMS_MESSAGE].stringValue
                self.view.makeToast(errorMsg)
            }
        }catch{
            self.view.makeToast(error.localizedDescription)
        }
    }
    
    public func parseRequestID(data:Data) -> Int {
        var requestID = 0
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if (status == Constants.PARAMS_PROCESSING){
                self.view.makeToast(Constants.PARAMS_SUCCESS)
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self.dismiss(animated: true, completion: nil)
                }
                requestID = json["request_id"].intValue
                
            } else if (status == Constants.PARAMS_SUCCESS){
                let errorMsg = json[Constants.PARAMS_RESULT][Constants.PARAMS_ERROR][0][Constants.PARAMS_MESSAGE].stringValue
                self.view.makeToast(errorMsg)
            }
        }catch{
            self.view.makeToast(error.localizedDescription)
        }
        return requestID
    }
    
    public func getHDMIAudioStatus(data:Data) -> Int {
        var result = 0
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if(status == Constants.PARAMS_SUCCESS){
                if let nodes = json[Constants.PARAMS_RESULT][Constants.PARAMS_DEVICES][0][Constants.PARAMS_NODES].array {
                    for node in nodes {
                        if(node[Constants.PARAMS_TYPE].stringValue == Constants.HDMI_ENCODER){
                            result = node["inputs"][1]["configuration"]["source"]["value"].intValue
                            break
                        }
                    }
                    
                }

            } else {
                self.view.makeToast(json[Constants.PARAMS_ERROR].stringValue)
            }
        }catch{
            self.view.makeToast(error.localizedDescription)
        }
        return result
    }
    
    public func parseI2SAudioStatus(data:Data) -> (String,Int,Int,Int) {
        var encodingType = ""
        var frequency = 0
        var channel = 0
        var i2Status = 0
        
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if(status == Constants.PARAMS_SUCCESS){
                if let nodes = json[Constants.PARAMS_RESULT][Constants.PARAMS_DEVICES][0][Constants.PARAMS_NODES].array {
                    for node in nodes {
                        if(node[Constants.PARAMS_TYPE].stringValue == "I2S_AUDIO_INPUT"){
                            frequency = node["configuration"]["sampling_frequency"]["value"].intValue
                            channel   = node["configuration"]["number_of_channels"]["value"].intValue
                            encodingType   = node["configuration"]["audio_encoding_type"].stringValue
                        }
                        
                        if(node[Constants.PARAMS_TYPE].stringValue == "I2S_AUDIO_OUTPUT"){
                             i2Status = node["inputs"][0]["configuration"]["source"]["value"].intValue
                        }
                        
                        
                    }
                    
                }

            } else {
                self.view.makeToast(json[Constants.PARAMS_ERROR].stringValue)
            }
        }catch{
            self.view.makeToast(error.localizedDescription)
        }
        return (encodingType,frequency,channel,i2Status)
    }
    
    public func parseUSBRole(data:Data) -> String{
        var usbRole = ""
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if(status == Constants.PARAMS_SUCCESS){
                if let nodes = json[Constants.PARAMS_RESULT][Constants.PARAMS_DEVICES][0][Constants.PARAMS_NODES].array {
                    for node in nodes {
                        if(node[Constants.PARAMS_TYPE].stringValue == "USB_HID"){
                            usbRole = node["configuration"]["role"].stringValue
                            break
                        }
                    }
                    
                }

            } else {
                self.view.makeToast(json[Constants.PARAMS_ERROR].stringValue)
            }
        }catch{
            self.view.makeToast(error.localizedDescription)
        }
        return usbRole
    }
    
    public func parseResolution(data:Data) -> (Int,Int){
        var width = 0
        var height = 0
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if(status == Constants.PARAMS_SUCCESS){
                if let nodes = json[Constants.PARAMS_RESULT][Constants.PARAMS_DEVICES][0][Constants.PARAMS_NODES].array {
                    for node in nodes {
                        if(node[Constants.PARAMS_TYPE].stringValue == "FRAME_BUFFER"){
                            width = node["status"]["source_video"]["width"].intValue
                            height = node["status"]["source_video"]["height"].intValue
                            break
                        }
                    }
                    
                }

            } else {
                self.view.makeToast(json[Constants.PARAMS_ERROR].stringValue)
            }
        }catch{
            self.view.makeToast(error.localizedDescription)
        }

        return (width,height)
    }
    
    public func parseEDID(data:Data , type:Int) -> String {
        var edid = ""
        do{
            let json = try JSON(data: data)
            let status = json[Constants.PARAMS_STATUS].stringValue
            if(status == Constants.PARAMS_SUCCESS){
                if let nodes = json[Constants.PARAMS_RESULT][Constants.PARAMS_DEVICES][0][Constants.PARAMS_NODES].array {
                    for node in nodes {
                        if(type == 0){
                            if(node[Constants.PARAMS_TYPE].stringValue == "HDMI_DECODER"){
                                edid = node["configuration"]["edid"].stringValue
                                break
                            }
                        } else {
                            if(node[Constants.PARAMS_TYPE].stringValue == "HDMI_MONITOR"){
                                edid = node["status"]["edid"].stringValue
                                break
                            }
                        }

                    }
                    
                }
            } else {
                self.view.makeToast(json[Constants.PARAMS_ERROR].stringValue)
            }
        }catch{
            self.view.makeToast(error.localizedDescription)
        }
        return edid
    }
}
