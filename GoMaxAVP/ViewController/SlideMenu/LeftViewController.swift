//
//  LeftViewController.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/3.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case deviceList = 0
    case changeSource
    case displayWall
    case hDMIAudio
    case i2SAudio
    case uSBRouting
    case eDID
    case settings
    case nonMenu
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var versionLabel: UILabel!
    
    var menus = [
        (name:"Device List",image:"nav_controller"),
        (name:"Change Source",image:"nav_controller"),
        (name:"Display Wall",image:"nav_tv_wall"),
        (name:"HDMI Audio",image:"nav_hdmi"),
        (name:"I2S Audio",image:"nav_earphones"),
        (name:"USB Routing",image:"usb"),
        (name:"EDID",image:"selection"),
        (name:"Settings",image:"nav_settings")]
    
    var deviceListViewController: UIViewController!
    var changeSourceViewController: UIViewController!
    var displayWallViewController: UIViewController!
    var hDMIAudioViewController: UIViewController!
    var i2SAudioViewController: UIViewController!
    var uSBRoutingViewController: UIViewController!
    var eDIDViewController: UIViewController!
    var settingViewController: UIViewController!
    var nonMenuViewController: UIViewController!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView();
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let deviceListViewController = storyboard.instantiateViewController(withIdentifier: DeviceListViewController.typeName) as! DeviceListViewController
        self.deviceListViewController = UINavigationController(rootViewController: deviceListViewController)
        
        let changeSourceViewController = storyboard.instantiateViewController(withIdentifier: ChangeSourceViewController
            .typeName) as! ChangeSourceViewController
        self.changeSourceViewController = UINavigationController(rootViewController: changeSourceViewController)
        
        let displayWallViewController = storyboard.instantiateViewController(withIdentifier: DisplayWallViewController.typeName) as! DisplayWallViewController
        self.displayWallViewController = UINavigationController(rootViewController: displayWallViewController)
        
        let hDMIAudioViewController = storyboard.instantiateViewController(withIdentifier: HDMIAudioViewController.typeName) as! HDMIAudioViewController
        self.hDMIAudioViewController = UINavigationController(rootViewController: hDMIAudioViewController)
        
        let i2SAudioViewController = storyboard.instantiateViewController(withIdentifier: I2SAudioViewController.typeName) as! I2SAudioViewController
        self.i2SAudioViewController = UINavigationController(rootViewController: i2SAudioViewController)
        
        let uSBRoutingViewController = storyboard.instantiateViewController(withIdentifier: USBRoutingViewController.typeName) as! USBRoutingViewController
        self.uSBRoutingViewController = UINavigationController(rootViewController: uSBRoutingViewController)
        
        let eDIDViewController = storyboard.instantiateViewController(withIdentifier: EDIDViewController.typeName) as! EDIDViewController
        self.eDIDViewController = UINavigationController(rootViewController: eDIDViewController)
        
        let settingViewController = storyboard.instantiateViewController(withIdentifier: SettingViewController.typeName) as! SettingViewController
        self.settingViewController = UINavigationController(rootViewController: settingViewController)
        

        
        let nonMenuController = storyboard.instantiateViewController(withIdentifier: "NonMenuController") as! NonMenuController
        nonMenuController.delegate = self
        self.nonMenuViewController = UINavigationController(rootViewController: nonMenuController)
        
//        self.tableView.registerCellClass(BaseTableViewCell.self)
        
        if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") {
            self.versionLabel.text = "APP Ver. \(version)"
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .deviceList:
            self.slideMenuController()?.changeMainViewController(self.deviceListViewController, close: true)
        case .changeSource:
            self.slideMenuController()?.changeMainViewController(self.changeSourceViewController, close: true)
        case .displayWall:
            self.slideMenuController()?.changeMainViewController(self.displayWallViewController, close: true)
        case .hDMIAudio:
            self.slideMenuController()?.changeMainViewController(self.hDMIAudioViewController, close: true)
        case .i2SAudio:
            self.slideMenuController()?.changeMainViewController(self.i2SAudioViewController, close: true)
        case .uSBRouting:
            self.slideMenuController()?.changeMainViewController(self.uSBRoutingViewController, close: true)
        case .eDID:
            self.slideMenuController()?.changeMainViewController(self.eDIDViewController, close: true)
        case .settings:
            self.slideMenuController()?.changeMainViewController(self.settingViewController, close: true)
        case .nonMenu:
            self.slideMenuController()?.changeMainViewController(self.nonMenuViewController, close: true)
        }
    }
}

extension LeftViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .deviceList, .changeSource, .displayWall,.hDMIAudio, .i2SAudio, .uSBRouting,.eDID, .settings, .nonMenu:
                return BaseTableViewCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .deviceList, .changeSource, .displayWall,.hDMIAudio, .i2SAudio, .uSBRouting,.eDID, .settings, .nonMenu:
//                let cell = BaseTableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: BaseTableViewCell.identifier)
                let cell = tableView.dequeueReusableCell(withIdentifier: "BaseTableViewCell", for: indexPath) as! BaseTableViewCell
                cell.setData(menus[indexPath.row])
                return cell
            }
        }
        return UITableViewCell()
    }
    
    
}
