//
//  OCFile.m
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/17.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

#import "OCFile.h"
#import "Edid.hpp"

@implementation OCFile

- (char *)parseEDID: (unsigned char *) buf withLen: (int)len{
    return testCC().edidParser(buf,len);
}

@end
