//
//  OCFile.h
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/17.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OCFile : NSObject
- (char *)parseEDID:(unsigned char *) buf withLen: (int)len;
@end

NS_ASSUME_NONNULL_END
