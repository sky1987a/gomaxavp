//
//  CoreDataManager.swift
//  GoMaxAVP
//
//  Created by DarrenHuang on 2020/6/23.
//  Copyright © 2020 DarrenHuang. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager {
    static let shared = CoreDataManager()
    
    private init() {}
    
    func insertCmd(cmd:String){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let cmdEntity = NSEntityDescription.entity(forEntityName: "Cmd", in: managedContext)!
        let c = NSManagedObject(entity: cmdEntity, insertInto: managedContext)
        c.setValue(cmd, forKey: "command")
        c.setValue(Int64(generateCurrentTimeStamp()), forKey: "created_time")

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func saveCmdToFile(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cmd")
        do{
            let result = try managedContext.fetch(fetchRequest)
            if result.count > 0 {
                var str = ""
                for cmd in result {
                    let c = cmd as! NSManagedObject
                    let command = c.value(forKey: "command") as! String
                    str += command
                }
                
                let file = AppFile()
                _ = file.writeFile(containing: str, to: .Documents, withName: "multiview.txt")
            }
        }catch{
            print(error.localizedDescription)
        }
        
    }
    
    func insertData(mac:String,name:String){
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Now let’s create an entity and new user records.
        let deviceEntity = NSEntityDescription.entity(forEntityName: "Device", in: managedContext)!
        
        //final, we need to add some data to our newly created record for each keys using
        //here adding 5 data with loop
        
        do{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Device")
            fetchRequest.predicate = NSPredicate(format: "mac==%@",mac)
            let result = try managedContext.fetch(fetchRequest)
            if (result.count == 0){
                // 新增
                let device = NSManagedObject(entity: deviceEntity, insertInto: managedContext)
                device.setValue(mac, forKey: "mac")
                device.setValue(name, forKey: "name")
            }else{
                let device = result.first as! NSManagedObject
                device.setValue(name, forKey: "name")
            }
            
        }catch{
            print(error.localizedDescription)
        }

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getDeviceName(mac:String) -> String {
        var name = ""
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return name}
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Device")
        fetchRequest.predicate = NSPredicate(format: "mac = %@", mac)
        do{
            let result = try managedContext.fetch(fetchRequest)
            if result.count > 0 {
                let device = result.first as! NSManagedObject
                name = device.value(forKey: "name") as! String
            } else {
                name = mac
            }
        }catch{
            print(error.localizedDescription)
            name = mac
            
        }
        return name
    }
    
//    func getDeviceMac(name:String) -> String {
//        var mac = ""
//        //As we know that container is set up in the AppDelegates so we need to refer that container.
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return name}
//        
//        //We need to create a context from this container
//        let managedContext = appDelegate.persistentContainer.viewContext
//        
//        //Prepare the request of type NSFetchRequest  for the entity
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Device")
//        fetchRequest.predicate = NSPredicate(format: "name = %@", name)
//        do{
//            let result = try managedContext.fetch(fetchRequest)
//            if result.count > 0 {
//                let device = result.first as! NSManagedObject
//                mac = device.value(forKey: "mac") as! String
//            } else {
//                mac = name
//            }
//        }catch{
//            print(error.localizedDescription)
//            mac = name
//            
//        }
//        return mac
//    }
    
    fileprivate func generateCurrentTimeStamp () -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmss"
        return (formatter.string(from: Date()) as NSString) as String
    }
}
