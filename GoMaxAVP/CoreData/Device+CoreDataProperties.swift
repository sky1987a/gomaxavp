//
//  Device+CoreDataProperties.swift
//  
//
//  Created by DarrenHuang on 2020/6/23.
//
//

import Foundation
import CoreData


extension Device {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Device> {
        return NSFetchRequest<Device>(entityName: "Device")
    }

    @NSManaged public var id: Int32
    @NSManaged public var mac: String?
    @NSManaged public var name: String?

}
